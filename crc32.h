#ifndef __CRC32_H__
#define __CRC32_H__

#include <AmiDxeLib.h>

unsigned long Crc32_ComputeBuf( 
   unsigned long inCrc32, 
   const void *buf,
   UINT32 bufLen
);

#endif
