
 There are some different kind of type datas need to be filled into AST memory
 space.  For example Voltage Type Data, has 4 banks. Each bank has 10 I/O buffers.

 Each I/O buffer specified how the register should be accessed by HWM FW, some
 bytes are opaque data for BIOS or EFI, just fill into memory space since the
 opaque data is provided from HWM FW Team.

 io_buff contains reading data no matter the bank is temperature or voltage.

 Since CPU temperature is from PECI, HWM FW  will convert the PECI GetTemp format
 into degree C, BIOS doest need to convert or other tempereatures come from PECI.

 Other I2C temp sensors is also depended on the chip, BIOS maybe need to known the
 sensor format from the chip.

 The ADC reading value is the raw value, BIOS need to *interpret* the ADC raw value
 into the source voltage value.  This is depended what's chip be used and resistors
 be used.


 The layout of the binary format is the follow:

 +--------------------------------+ \
 | Voltage Data    (4 Banks))     |   For Voltage Data, Temp Data, PWM Data, Fan Data, I2c Init Data banks
 |                                |   Each bank byte size is sizeof(ec)
 +--------------------------------+ /
 | Tempereature Data  (4 Banks)   | .
 |                                | .
 +--------------------------------+ .
 | PWM Data (4 Banks)             | .
 |                                | .
 +--------------------------------+ .
 | Fan Data(Tacho) (4 Banks)      | .
 |                                | .
 +--------------------------------+ .
 | I2C init data (4 Banks)        | .
 |                                |
 +--------------------------------+ \
 | FSC Table (5 Banks)            |
 |                                |  The FSC Table bank byte size is sizeof(fsc_sm)
 +--------------------------------+ /


 Check dtbb.h to see the struct definition.

