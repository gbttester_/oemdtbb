#ifndef __SNB_H__
#define __SNB_H__
#ifdef APTIO
#include "token.h"
#include <AmiLib.h>
#include <AmiDxeLib.h>
#endif

#if DYNAMIC_PCIEXBASE_SUPPORT
#include "DynamicPciBase.h"
#endif

typedef UINT8	uint8_t;
typedef UINT16	uint16_t;
typedef UINT32	uint32_t;
typedef UINT64	uint64_t;

// CPUBUS(1), Device:10,	Function:0, Offset:0x84
typedef union {
	uint64_t val;
	struct {
		uint64_t	package_tdp_power:15;
		uint64_t	reserved01:1;
		uint64_t	package_min_power:15;
		uint64_t	reserved02:1;
		uint64_t	package_max_power:15;
		uint64_t	reserved03:1;
		uint64_t	package_max_time:7;
		uint64_t	reserved04:9;
	};
} package_power_sku;

// CPUBUS(1), Device:10,	Function:0, Offset:0x8C
typedef union {
	uint32_t val;
	struct {
		uint32_t	power_unit:4;		// Power Unit = 1 / (2 ^ POWER_UNIT) W
		uint32_t	reserved01:4;
		uint32_t	engery_unit:4;		// Energy Unit = 1 / (2 ^ ENERGY_UNIT) J
		uint32_t	reserved02:4;
		uint32_t	time_unit:12;		// Time Unit = 1 / (2 ^ TIME_UNIT) seconds
	};
} package_power_sku_unit;

uint64_t read_tjmax(uint8_t node);
uint32_t read_tdp(uint8_t node);

typedef INT16 cpu_temp_t;

#ifndef _INTERPRET_CPU_TEMP
#define _INTERPRET_CPU_TEMP(_temp, _tjmax)		\
	(UINT32)((((double)((cpu_temp_t)_temp)) / 64) + (_tjmax))
#endif

#endif

