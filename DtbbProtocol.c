#include "OemDtbb.h"
#include "DtbbProtocol.h"

#ifndef offsetof
#define offsetof(st, m) \
     ((UINTN) ( (char *)&((volatile  st *)0)->m - (volatile  char *)0 ))
#endif

#undef EFI_DEBUG
#define EFI_DEBUG       1

EFI_STATUS
EFIAPI
OemGetSharedAddress(
    IN OEM_DTBB_PROTOCOL *This,
    OUT UINT32 *Address,
    OUT UINT32 *Length
)
{
    EFI_STATUS      Status;

    Status = GetSharedAddress(Address, Length);
    return Status;
}

EFI_STATUS
EFIAPI
OemMapSharedRegion(
  IN OEM_DTBB_PROTOCOL *This,
  IN UINT32 Address,
  OUT UINT32 *HostAddress
)
{
    EFI_STATUS  Status;

    Status = MapShareRegion(Address, HostAddress);
    return Status;
}

EFI_STATUS
EFIAPI
OemHitDoorRing(
    IN OEM_DTBB_PROTOCOL *This,
    IN UINT16 Length
)
{
    EFI_STATUS      Status;

    Status = DoorRingFinished(Length);
    return Status;
}

EFI_STATUS DtbbHelper_GetVersionInfo(
   IN OEM_DTBB_PROTOCOL     *This,
   OUT ver_info             **VerInfo
   )
{
    EFI_STATUS              Status;
    bios_form               *volatile bf;
#if EFI_DEBUG
    UINT32          Sdr;
    UINT32          Fru;
    UINT32          Fw;
#endif

    if (VerInfo == NULL) {
        return EFI_INVALID_PARAMETER;
    }

    TRACE((-1, "SharedAddr:%x\n", mSharedAhbAddr));

    Status = MapShareRegion((UINT32)mSharedAhbAddr, (UINT32*)&bf);
    if (EFI_ERROR(Status)) {
        TRACE((-1, "Cant Map share region, Status:%x\n", Status));
    } else {
        TRACE((-1, "Map share region okey!"));
    }

    TRACE((-1, "bf:%x\n", bf));
    pBS->SetMem((void*)bf, sizeof(bios_form), 0);
    bf->version.action = DTBB_ACTION_GET;
    INIT_CC_FIELD(bf->version);

    Status = DoorRingFinished(0);

    WAIT_FOR_DTBB_REQ(bf->version);
    if (bf->version.compcode && Status == 0) {
        Status = EFI_DEVICE_ERROR;
    }
#if EFI_DEBUG
    TRACE((-1, "DoorbellRing,Status:%x\n", Status));
    TRACE((-1, "OEM CC: %x\n",  bf->version.compcode));

    Sdr = *(volatile UINT32*)&bf->version.un.ver.sdr[0];
    Fw = *(volatile UINT32*)&bf->version.un.ver.fw[0];
    Fru = *(volatile UINT32*)&bf->version.un.ver.fru[0];
    TRACE((-1, "FwVer:%x\n", Fw));
    TRACE((-1, "FruVer:%x\n", Fru));
    TRACE((-1, "SdrVer:%x\n", Sdr));
#endif

    *VerInfo = &bf->version.un.ver;
    bf->version.action = DTBB_ACTION_NULL;

    return Status;
}

EFI_STATUS DtbbHelper_GetBmcNetworkRequest(
   IN OEM_DTBB_PROTOCOL *This,
   IN OUT  ip_conf      **IpConf
)
{
    EFI_STATUS  Status;
    bios_form       *bf2;

    if (IpConf == NULL) {
        return EFI_INVALID_PARAMETER;
    }

    Status = MapShareRegion((UINT32)mSharedAhbAddr, (UINT32*)&bf2);
    if (EFI_ERROR(Status)) {
        TRACE((-1, "Cant Map share region, Status:%x\n", Status));
    } else {
        TRACE((-1, "Map share region okey!"));
        pBS->SetMem((void*)bf2, sizeof(bios_form), 0);
        *IpConf = &bf2->ipv4.un.ip;
    }
    
    return Status;
}


void __inline hexdump(UINT8 *buf, UINT32 length)
{
    UINTN i;

    for (i=0; i != length; i++) {
        DEBUG_FUNC((0xffffffff, "%02x ", buf[i]));
        if (i!=0 && (i+1)%16 == 0) {
            DEBUG_FUNC((0xffffffff, "\n"));
        }
    }
    if (!(i!=0 && (i+1)%16 == 0)) {
            DEBUG_FUNC((0xffffffff, "\n"));
    }
}

EFI_STATUS DtbbHelper_SubmitBmcNetworkRequest(
   IN OEM_DTBB_PROTOCOL *This,
   IN UINT8             Action,
   IN ip_conf           *IpConf
)
{
    EFI_STATUS  Status;
    bios_form       * volatile bf2;
#if EFI_DEBUG
    UINT32      ip_addr;
    UINT32      gateway;
    UINT32      netmask;
    UINT8       ip_src;
#endif

    if (IpConf == NULL) {
        return EFI_INVALID_PARAMETER;
    }
    
    bf2 = (bios_form *)(((UINT8 *)IpConf - offsetof(share_memory, un.ip)) - 
                         offsetof(bios_form, ipv4));

    TRACE((-1, "bf2: %x\n", bf2));
    TRACE((-1, "ipv4: %x\n", &bf2->ipv4));


    // INIT_CC_FIELD(bf2->ipv4);
    bf2->ipv4.action = Action;
    bf2->ipv4.compcode = 0;
    bf2->ipv4.mode = 0;

    hexdump((UINT8*) &bf2->ipv4, 0x60);
#if EFI_DEBUG
    ip_addr = *(volatile UINT32 *)(&bf2->ipv4.un.ip.ip_addr);
    netmask = *(volatile UINT32 *)(&bf2->ipv4.un.ip.netmask);
    gateway = *(volatile UINT32 *)(&bf2->ipv4.un.ip.gateway);
    ip_src = bf2->ipv4.un.ip.lan_info[1];

    TRACE((-1, " ipv4_action:%x\n", bf2->ipv4.action));
    TRACE((-1, " OEM CC: %x\n", bf2->ipv4.compcode));
    TRACE((-1, " lan_info[0]: %x\n", bf2->ipv4.un.ip.lan_info[0]));
    TRACE((-1, " lan_info[1]: %x\n", bf2->ipv4.un.ip.lan_info[1]));

    TRACE((-1, " ip_src: %x\n", ip_src));
    TRACE((-1, " ip_addr:%x\n", ip_addr));
    TRACE((-1, " netmask:%x\n", netmask));
    TRACE((-1, " gateway:%x\n", gateway));
#endif

    Status = DoorRingFinished(0);

    // WAIT_FOR_DTBB_REQ(bf2->ipv4);

    if (bf2->ipv4.compcode && Status == 0) {
        Status = EFI_DEVICE_ERROR;
    }

#if EFI_DEBUG
    ip_addr = *(volatile UINT32 *)(&bf2->ipv4.un.ip.ip_addr);
    netmask = *(volatile UINT32 *)(&bf2->ipv4.un.ip.netmask);
    gateway = *(volatile UINT32 *)(&bf2->ipv4.un.ip.gateway);
    ip_src = bf2->ipv4.un.ip.lan_info[1];

    TRACE((-1, "OEM CC: %x\n", bf2->ipv4.compcode));
    TRACE((-1, "lan_info[0]: %x\n", bf2->ipv4.un.ip.lan_info[0]));
    TRACE((-1, "lan_info[1]: %x\n", bf2->ipv4.un.ip.lan_info[1]));
    TRACE((-1, "ip_src: %x\n", ip_src));
    TRACE((-1, "ip_addr:%x\n", ip_addr));
    TRACE((-1, "netmask:%x\n", netmask));
    TRACE((-1, "gateway:%x\n", gateway));
#endif

    return Status;
}


/** 
 * @brief To retrieve all FRU datas from BMC.
 * 
 * @param This          -
 * @param FruData       - FRU raw data.
 * @param FruDataSize   - FRU raw data length in bytes
 * 
 * @return EFI_STATUS
 */
EFI_STATUS DtbbHelper_GetFruData(
   IN OEM_DTBB_PROTOCOL     *This,
   IN OUT UINT8             **FruData,
   IN OUT UINTN             *FruDataSize
)
{
    EFI_STATUS  Status;
    bios_form   *bf;
    UINT8       *FruDataAddr;

    if (FruData == NULL) {
        return EFI_INVALID_PARAMETER;
    }

    Status = MapShareRegion((UINT32)mSharedAhbAddr, (UINT32*)&bf);
    if (EFI_ERROR(Status)) {
        TRACE((-1, "Cant Map share region, Status:%x\n", Status));
    } else {
        TRACE((-1, "Map share region okey!"));
    }

    TRACE((-1, "bf:%x\n", bf));
    pBS->SetMem((void*)bf, sizeof(bios_form), 0);
    bf->req_info.action = DTBB_ACTION_GET;
    bf->req_info.un.td.size = 0;
    bf->req_info.un.td.value[0] = 2;

    INIT_CC_FIELD(bf->req_info);
    Status = DoorRingFinished(0);

    WAIT_FOR_DTBB_REQ(bf->req_info);
    if (bf->req_info.compcode && Status == 0) {
        Status = EFI_DEVICE_ERROR;
    }
    if (EFI_ERROR(Status)) {
        return Status;
    }

#if EFI_DEBUG
    TRACE((-1, "DoorbellRing,Status:%x\n", Status));
    TRACE((-1, "GET FRU OEM CC: %x\n",  bf->req_info.compcode));

    TRACE((-1, "FruDataLen: %x\n", bf->req_info.un.req.fru_len));
    TRACE((-1, "FruDataAddr: %x\n", bf->req_info.un.req.fru_index));

    TRACE((-1, "DataLen: %x\n", bf->req_info.un.req.sel_len));
    TRACE((-1, "DataAddr: %x\n", bf->req_info.un.req.sel_index));
#endif

    FruDataAddr = (UINT8 *)(bf->req_info.un.req.fru_index);
    *FruDataSize = bf->req_info.un.req.fru_len;

    Status = MapShareRegion((UINT32)FruDataAddr, (UINT32*)FruData);
    if (EFI_ERROR(Status)) {
        TRACE((-1, "Cant Map share region, Status:%x\n", Status));
    } else {
        TRACE((-1, "Map share region okey! FruData:%x\n", FruData));
    }


    return Status;
}


/** 
 * @brief To retrieve all SEL datas from BMC.
 * 
 * @param This          
 * @param SelData       - SEL raw data 
 * @param SelDataSize   - SEL raw data length in bytes
 * 
 * @return EFI_STATUS   
 */
EFI_STATUS DtbbHelper_GetSelData(
   IN OEM_DTBB_PROTOCOL     *This,
   IN OUT UINT8             **SelData,
   IN OUT UINTN             *SelDataSize
)
{
    EFI_STATUS  Status;
    bios_form   *bf;
    UINT8       *SelDataAddr;

    if (SelData == NULL) {
        return EFI_INVALID_PARAMETER;
    }

    Status = MapShareRegion((UINT32)mSharedAhbAddr, (UINT32*)&bf);
    if (EFI_ERROR(Status)) {
        TRACE((-1, "Cant Map share region, Status:%x\n", Status));
    } else {
        TRACE((-1, "Map share region okey!"));
    }

    TRACE((-1, "bf:%x\n", bf));
    pBS->SetMem((void*)bf, sizeof(bios_form), 0);
    bf->req_info.action = DTBB_ACTION_GET;
    bf->req_info.un.td.size = 0;
    bf->req_info.un.td.value[0] = 3;

    INIT_CC_FIELD(bf->req_info);
    Status = DoorRingFinished(0);

    WAIT_FOR_DTBB_REQ(bf->req_info);
    if (bf->req_info.compcode && Status == 0) {
        Status = EFI_DEVICE_ERROR;
    }
    if (EFI_ERROR(Status)) {
        return Status;
    }

    SelDataAddr = (UINT8*) bf->req_info.un.req.sel_index;
    *SelDataSize = bf->req_info.un.req.sel_len;

#if EFI_DEBUG
    TRACE((-1, "DoorbellRing,Status:%x\n", Status));
    TRACE((-1, "Get SEL OEM CC: %x\n",  bf->req_info.compcode));

    TRACE((-1, "FruDataLen: %x\n", bf->req_info.un.req.fru_len));
    TRACE((-1, "FruDataAddr: %x\n", bf->req_info.un.req.fru_index));

    TRACE((-1, "SdrDataLen: %x\n", bf->req_info.un.req.sel_len));
    TRACE((-1, "SdrDataAddr: %x\n", bf->req_info.un.req.sel_index));
#endif

    TRACE((-1, "Req\n"));
    hexdump((UINT8*)&bf->req_info, 256);

    Status = MapShareRegion((UINT32)SelDataAddr, (UINT32*)SelData);
    if (EFI_ERROR(Status)) {
        TRACE((-1, "Cant Map share region, Status:%x\n", Status));
    } else {
        TRACE((-1, "Map share region okey!"));
    }



    return Status;
}

EFI_STATUS DtbbHelper_test(VOID)
{
    EFI_STATUS          Status;
    // ip_conf              *ip_conf;
    UINTN               SelLen;
    UINTN               FruLen;
    UINT8               *SelData;
    UINT8               *FruData;

    #if 0
    Status = DtbbHelper_GetBmcNetworkRequest(NULL, &ip_conf);
    if (EFI_ERROR(Status)) {
        TRACE((-1, "GetBmcNetworkReq failed, Status:%x\n", Status));
    }

    TRACE((-1, "ip_conf:%x\n", ip_conf));

    ip_conf->lan_info[0] = 1;
    ip_conf->lan_info[1] = 1; /* static */

    ip_conf->gateway[0] = 10;
    ip_conf->gateway[1] = 10;
    ip_conf->gateway[2] = 27;
    ip_conf->gateway[3] = 253;

    ip_conf->ip_addr[0] = 10;
    ip_conf->ip_addr[1] = 10;
    ip_conf->ip_addr[2] = 27;
    ip_conf->ip_addr[3] = 47;

    ip_conf->netmask[0] = 255;
    ip_conf->netmask[1] = 0;
    ip_conf->netmask[2] = 0;
    ip_conf->netmask[3] = 0;
    
    Status = DtbbHelper_SubmitBmcNetworkRequest(NULL, DTBB_ACTION_SET, ip_conf);
    if (EFI_ERROR(Status)) {
        TRACE((-1, "SumbitBmcNetReq failed, Status:%x\n", Status));
    }
    #endif

    Status = DtbbHelper_GetFruData(NULL, &FruData, &FruLen);
    TRACE((-1, "FruData:%x FruLen:%x\n", FruData, FruLen));
    hexdump(FruData, (UINT32) FruLen);

    (void) SelData;
    (void) SelLen;

    Status = DtbbHelper_GetSelData(NULL, &SelData, &SelLen);

    TRACE((-1, "SelLen:%x SelData:%x\n", SelLen, SelData));

    hexdump(SelData, (UINT32) SelLen);

    #if 0
    pBS->Stall(1000* 9000);
      
    TRACE((-1, "begin to get, ip_conf: %x\n", ip_conf));
    pBS->SetMem(ip_conf, sizeof(ip_conf), 0);
    ip_conf->lan_info[0] = 1;
    Status = DtbbHelper_SubmitBmcNetworkRequest(NULL, DTBB_ACTION_GET, ip_conf);

    if (EFI_ERROR(Status)) {
        TRACE((-1, "Get Bmc Ipconfig failed, Status:%x\n", Status));
    }
    #endif

    return Status;
}

static
OEM_DTBB_PROTOCOL mOemDtbbProtocol = {
    OEM_DTBB_PROTOCOL_VERSION,
    OemGetSharedAddress,
    OemMapSharedRegion,
    OemHitDoorRing
};


static
EFI_GUID    mOemDtbbGuid = OEM_DTBB_PROTOCOL_GUID;

EFI_STATUS
InstallDtbbProtocol(VOID)
{
    EFI_STATUS Status;
    EFI_HANDLE ProtoHandle = NULL;

    Status = pBS->InstallMultipleProtocolInterfaces(
            &ProtoHandle,
            &mOemDtbbGuid,
            &mOemDtbbProtocol,
            NULL
            );

    if (EFI_ERROR(Status)) {
        DEBUG_FUNC((-1, "Can't Install Dtbb Protocol, Status:%x\n", Status));
    }

    return Status;
}
