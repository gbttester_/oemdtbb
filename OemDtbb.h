#ifndef __SEND_IPMI_BIOS_VERSION__
#define  __SEND_IPMI_BIOS_VERSION__

#define _SMIBios_Dynamic_H

#include <Token.h>
#include <AmiDxeLib.h>

#if DYNAMIC_PCIEXBASE_SUPPORT
#include "DynamicPciBase.h"
#endif

//#include "Ipmi/Ipmi.h"

#include "Protocol/Variable.h"
#include "Protocol/PciIo.h"
#include "Include/PciBusEx.h"
#include "dtbb.h"

static EFI_GUID gAmiExtPciBusProtocolGuid = AMI_PCI_BUS_EXT_PROTOCOL_GUID;

#define EFI_STRINGIZE(a)            #a
#define DEBUG_FUNC                  TRACE

#ifndef USE_KCS_TO_GET_SHARE_ADDR
#define USE_KCS_TO_GET_SHARE_ADDR       (0)
#endif


typedef VOID (SEND_IPMI_SMBIOS_FUNCTION)(VOID);
#ifdef __cplusplus
extern "C" {
#endif

#ifndef EFI_SM_APPLICATION
#define EFI_SM_APPLICATION  0x6
#endif

#ifndef EFI_SM_OEM_GROUP
#define EFI_SM_OEM_GROUP    0x2E
#endif

#ifndef CMD_GET_SYSTEM_INFO
#define CMD_GET_SYSTEM_INFO 0x59
#endif

#ifndef CMD_SET_SYSTEM_INFO
#define CMD_SET_SYSTEM_INFO 0x58
#endif

#ifndef CMD_GET_SHARED_REGION
#define CMD_GET_SHARED_REGION 0xb1
#endif

#ifndef CMD_RING_SHARED_REGION
#define CMD_RING_SHARED_REGION 0xb0
#endif



#ifndef EFI_IPMI_TRANSPORT_PROTOCOL_GUID
#define EFI_IPMI_TRANSPORT_PROTOCOL_GUID \
  { \
    0x6bb945e8, 0x3743, 0x433e, 0xb9, 0xe, 0x29, 0xb3, 0xd, 0x5d, 0xc6, 0x30 \
  }
#endif

//extern EFI_IPMI_TRANSPORT  *mIpmiTransportProtocol;

/*
 * Shared Memory Address will be static always.
 */
extern UINT32 mSharedAhbAddr;
extern UINT32 mSharedAhbSize;
extern UINT32 mWindow;


void hexdump(UINT8 *buf, UINT32 length);

EFI_STATUS EFIAPI MapShareRegion(
    IN UINT32 Dst,
    IN OUT UINT32 *HostAddress
);

EFI_STATUS EFIAPI GetSharedAddress(OUT UINT32 *Address, OUT UINT32 *Length);

EFI_STATUS EFIAPI CopyToSharedRegion(
    IN UINT32 *Src,
    IN UINT32 Dst,
    IN UINT32 Length);

EFI_STATUS EFIAPI DoorRingFinished(UINT16 Length);

EFI_STATUS EFIAPI InstallDtbbProtocol(VOID);

EFI_STATUS EFIAPI OemDtbbInit(VOID);

void do_all_test(void);



#ifdef __cplusplus
}
#pragma pack(pop)
#endif

#pragma pack(push, 1)
#pragma pack(pop)


#ifndef _TO_AHB_ADDR
#define _TO_AHB_ADDR(_x)	\
	((((UINT32)(_x)) - mWindow + mSharedAhbAddr))
#endif


#define     SYS_INFO_SEL_SIP                    0
#define     SYS_INFO_SEL_SYSTEM_FW              1
#define     SYS_INFO_REVISION                   0x11

#ifndef _FILL_ACTION
#define  _FILL_ACTION(_bf, _mem, _ele_size, _action)	\
do {\
	{\
		UINT32 i;\
		for (i = 0; i != (_ele_size); i++) {\
			(_bf)->_mem[i].action = (_action);\
			(_bf)->_mem[i].un.ec.id[0] = 'B';\
			(_bf)->_mem[i].un.ec.id[1] = 'I';\
			(_bf)->_mem[i].un.ec.id[2] = 'O';\
			(_bf)->_mem[i].un.ec.id[3] = 'S';\
		}\
	}\
}while (FALSE);


#define DTBB_TRACE

#if !defined(EFI_DEBUG) || !defined(DTBB_TRACE)
	#define __MY_TRACE
#else
	#define __MY_TRACE		TRACE
#endif

#if !defined(_ZERO_IO_ITEM_SIZE)
#define _ZERO_IO_ITEM_SIZE      (16)
#endif


#if !defined(_DTBB_NOT_USED_FLAG)
    #define _DTBB_NOT_USED_FLAG     (0xff)
#endif

#if !defined(_ZERO_IO_ITEM)
#define _ZERO_IO_ITEM(_bf, _name, _bank, _ctrl_idx)		\
	( __MY_TRACE((-1, "Zero16Bytes(%08x)\n", _TO_AHB_ADDR(&((_bf)->_name[(_bank)].un.ec.io_buff[(_ctrl_idx)])))) \
	 , pBS->SetMem(&((_bf)->_name[(_bank)].un.ec.control[(_ctrl_idx)]), _ZERO_IO_ITEM_SIZE, 0) \
     , ((_bf)->_name[(_bank)].un.ec.control[(_ctrl_idx)].ex_idx = _DTBB_NOT_USED_FLAG)\
	)
#endif

#endif
#endif
