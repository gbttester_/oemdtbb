#ifndef __ASPEED_HWM_BOARD_H__
#define __ASPEED_HWM_BOARD_H__
#include <Token.h>
#include <AmiLib.h>
#include <AmiDxeLib.h>
#include <Setup.h>
#include "Token.h"

#if TSE_APTIO_5_SUPPORT == 0
    #include <SetupStrTokens.h>
    #include "Board\IO\IT8728\IT8728Setup.H"
#endif

#define APTIO		1
#define INTERNAL	1

#include "ast1300.h"
#include "AspeedHwm.h"

extern
VOID InitString(EFI_HII_HANDLE HiiHandle, STRING_REF StrRef, CHAR16 *sFormat, ...);

typedef enum AstHwmReadingType {
    ReadingAdcType = 0,
    ReadingTempType,
    ReadingFanType
} AST_HWM_READING_TYPE;

typedef struct _AST_HWM_ITEM_ENTRY {
    AST_HWM_READING_TYPE  Type;
    UINT8                 ReadingIndex;
    CHAR8                 *DescString;
    UINT32                Flags;
    UINT32                PrivateData;
} AST_HWM_ITEM_ENTRY;

#define HWM_FLAGS_AST_ADC           (1)
#define HWM_FLAGS_ITE_ADC           (2)

UINT16 read_tach(UINT8);
UINT32 get_voltage_value(UINT8);
UINT16 it8728_read_vbat(UINT8);
UINT16 read_peci_temp(UINT8);
UINT16 read_i2c_temp(UINT8, UINT8);
UINT16 read_dimm_temp(UINT8 idx);

UINT8 is_adt7462_present(void);
UINT8 is_adt7476_present(void);

VOID AspeedHwmInit(
   EFI_HII_HANDLE		HiiHandle,
   UINT16				Class
);

extern VOID HHMCommon(
    IN      UINTN     RegData,
    IN      UINT8     Func,
    IN      UINT16    StrToken,
    IN      UINT8     OddPos,
    IN      EFI_HII_HANDLE    hiiHandle);

#if !defined(_ASPEED_HWM_RCONF_)
static
const double r_conf[][2]= {
    {5.11, 1.02},           // P12V
    {5.11, 3.48},           // P5V
    {5.11, 8.25},           // P3.3
    {5.11,  3.48},          // P5V_STBY
	{10,	0},				// P1.1 (SSB)
    {200, 100},             // VBAT
    {10, 0},                // CPU0 Vcc
    {10, 0},                // CPU1 Vcc
    {10, 0},                // PVDDQ_ABC
    {10, 0},                // PVDDQ_DEF
    {10, 0},                // PLL_CPU0
    {10, 0}                 // PLL_CPU1 (+11)
};
#else
static
const double r_conf[][2]= {
   _ASPEED_HWM_RCONF_
};
#endif

#endif
