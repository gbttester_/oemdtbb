#include "AspeedHwmBoard.h"
#include "OemDtbbSetupPageItem.h"

AST_HWM_ITEM_ENTRY HwmItems[] = {
    OEM_DTBB_ITEM_LIST
};

VOID AspeedStringInit(
   EFI_HII_HANDLE       HiiHandle,
   UINT16               Class
);

VOID InitItemNr(VOID)
{
    EFI_STATUS  Status;
    UINTN       VarSize = sizeof(SETUP_DATA);
    static SETUP_DATA   SetupData;
    static EFI_GUID     SetupGuid = SETUP_GUID;
    UINT8               entrySz;

    Status = pRS->GetVariable(
       L"Setup",
       &SetupGuid,
       NULL,
       &VarSize,
       &SetupData);

    if (EFI_ERROR(Status)) {
        TRACE((-1,  "Failed to GetVar, Status:%x\n", Status));
        return;
    }

    entrySz = sizeof(HwmItems) / sizeof(AST_HWM_ITEM_ENTRY);
    SetupData.AstHwmItemNum = entrySz;

    TRACE((-1, "AstHwmItemNum:%xh\n", entrySz));

    Status = pRS->SetVariable(
          L"Setup",
          &SetupGuid,
          EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
          VarSize,
          &SetupData);

    if (EFI_ERROR(Status)) {
        TRACE((-1,  "Failed to SetVar, Status:%x\n", Status));
        return;
    }
}

VOID AspeedHwmInit(
   EFI_HII_HANDLE       HiiHandle,
   UINT16               Class
)
{
    EFI_STATUS      Status;

    Status = OemDtbbInit();

    TRACE((-1, "Class:%x\n", Class));
    if (Class != ADVANCED_FORM_SET_CLASS) {
        return;
    }

    InitItemNr();
    AspeedStringInit(HiiHandle, Class);

    return;
}

VOID AspeedStringInit(
   EFI_HII_HANDLE       HiiHandle,
   UINT16               Class
)
{
    UINT8       index;
    __FP_USED__;

    for (index = 0; index != sizeof(HwmItems)/sizeof(AST_HWM_ITEM_ENTRY); index++) {
        InitString(
            HiiHandle,
            STRING_TOKEN(STR_OEM_AST_HWM_ITEM_00) + index * 2,
            L"%a",
           HwmItems[index].DescString);
    }
}

EFI_STATUS PreGetReading(
   AST_HWM_READING_TYPE	Type,
   UINT8 Index
)
{
	 bios_form  *bf;
	 UINT32      addr;
	 EFI_STATUS	Status;

	 Status = MapShareRegion(mSharedAhbAddr, &addr);

	 if (EFI_ERROR(Status)) {
        return 0;
	 }
	 bf = (bios_form *)addr;

	_FILL_ACTION(bf, sio_vin, SIO_EC_EACH_BANKS, SM_ACTION_GET);
	_FILL_ACTION(bf, sio_temp, SIO_EC_EACH_BANKS, SM_ACTION_GET);
	_FILL_ACTION(bf, sio_fan, SIO_EC_EACH_BANKS, SM_ACTION_GET);

	return 0;
}

UINT16 GetReading(
    AST_HWM_READING_TYPE Type,
    UINT8 Index
)
{
    bios_form  *bf;
    UINT32      addr;
    EFI_STATUS  Status;

    Status = MapShareRegion(mSharedAhbAddr, &addr);

    if (EFI_ERROR(Status)) {
        return 0;
    }
    bf = (bios_form *)addr;

    switch (Type) {
        case ReadingAdcType:
            return _HWM_GET_VALUE(bf, sio_vin, Index / NR_CTRL_PER_BANK, Index % NR_CTRL_PER_BANK);
            break;
        case ReadingTempType:
            return _HWM_GET_VALUE(bf, sio_temp, Index / NR_CTRL_PER_BANK, Index % NR_CTRL_PER_BANK);
            break;
        case ReadingFanType:
            return _HWM_GET_VALUE(bf, sio_fan, Index / NR_CTRL_PER_BANK, Index % NR_CTRL_PER_BANK);
            break;
        default:
            TRACE((-1, "ERROR: Type is wrong\n"));
            EFI_DEADLOOP();
    }

    return 0;
}

#define VOLTAGE             0x01
#define TEMPERATURE         0x02
#define FAN_SPEED           0x03

#define _GET_STR_TYPE_BY_READING_TYPE(_T) \
    ((_T) == ReadingAdcType ? (VOLTAGE) :\
    (_T) == ReadingTempType ? (TEMPERATURE) :\
    (_T) == ReadingFanType ? (FAN_SPEED) :  0)

void AspeedHwmStringCallback(
    IN EFI_HII_HANDLE HiiHandle,
    IN UINT16 Class,
    IN UINT16 SubClass,
    IN UINT16 Key
)
{
    EFI_STATUS Status;
    STRING_REF Token;        //String token
    UINT8      OddPos;       //Define how much is odd
	UINT64     data_value;
	UINT8      index;
	// EFI_TIME   time;
	UINT8      adc_idx;
	static UINT8 initDone = FALSE;
	static EFI_GUID DataProtoGuid = HWM_INIT_DATA_PROTOCOL_GUID;
	LOAD_TABLE_CONTEXT *ctx = NULL;

	if (Class != ADVANCED_FORM_SET_CLASS) {
		return;
	}

#if _OEM_DTBB_DISABLED
    return;
#endif

	if (!initDone) {
		Status = pBS->LocateProtocol(&DataProtoGuid, NULL, &ctx);
		initDone = TRUE;
		if (!EFI_ERROR(Status)) {
			TRACE((-1, "Crc32:%x\n", ctx->Crc32Value));
			InitString(
			   HiiHandle,
			   STRING_TOKEN(STR_CRC32_VALUE),
			   L"%08x",
			   ctx->Crc32Value);
		}
	}

	Status = 0;
	// pRS->GetTime(&time, NULL);
	// TRACE((-1, "TS: %d:%d:%d %d.%d.%d\n", time.Year, time.Month, time.Month, time.Hour, time.Minute, time.Second));
	// TRACE((-1, "==> %s\n", __FUNCTION__));

	PreGetReading(0, 0);
	for (index = 0; index != sizeof(HwmItems) / sizeof(AST_HWM_ITEM_ENTRY); index++) {
		data_value = GetReading(HwmItems[index].Type, HwmItems[index].ReadingIndex);
		if (HwmItems[index].Flags & HWM_FLAGS_AST_ADC) {
			OddPos = 3;
			adc_idx = HwmItems[index].PrivateData;
			TRACE((-1, "adc %d = %x\n", adc_idx, data_value));
			data_value = (UINT32)(ADC_INTERPRET3(r_conf[adc_idx][0], r_conf[adc_idx][1], (double)(data_value)));
		} else if (HwmItems[index].Flags & HWM_FLAGS_ITE_ADC) {
			OddPos = 3;
			data_value = (UINT32)(ADC_ITE_INTERPRET((double)(data_value)));
        } else {
            OddPos = 0;
        }
        if ((UINT16)data_value != 0xffff) {
            Token = STRING_TOKEN(STR_OEM_AST_HWM_ITEM_00_VALUE) + index*2;
			if (HwmItems[index].Type == ReadingFanType) {
                TRACE((-1, "Tacho x: %x\n", (UINT32)data_value));
			}
            HHMCommon((UINT32)data_value, _GET_STR_TYPE_BY_READING_TYPE(HwmItems[index].Type), Token, OddPos, HiiHandle);
        }
    }
}
