//*************************************************************************
//*************************************************************************
//**                                                                     **
//**        (C)Copyright 1985-2009, American Megatrends, Inc.            **
//**                                                                     **
//**                       All Rights Reserved.                          **
//**                                                                     **
//**      5555 Oakbrook Parkway, Suite 200, Norcross, GA 30093           **
//**                                                                     **
//**                       Phone: (770)-246-8600                         **
//**                                                                     **
//*************************************************************************
//*************************************************************************
//
//*************************************************************************
// $Header: /Alaska/BIN/IO/ITE/IT8728F/IT8728Setup.C 2     9/06/11 10:51p Ruidai $
//
// $Revision: 2 $
//
// $Date: 9/06/11 10:51p $
//*************************************************************************
// Revision History
// ----------------
// $Log: /Alaska/BIN/IO/ITE/IT8728F/IT8728Setup.C $
//
// 2     9/06/11 10:51p Ruidai
// Update to new templet.
//
// 1     4/06/10 11:14p Mikes
// Initial check-in
//
//*************************************************************************
//<AMI_FHDR_START>
//
// Name:  <IT8728Setup.C>
//
// Description: Setup related Routines.
//
//<AMI_FHDR_END>
//*************************************************************************

#include <Efi.h>
#include <AmiDxeLib.h>

#define STR_BUFFER_LENGTH   0x10
//Defination of function
#define VOLTAGE             0x01
#define TEMPERATURE         0x02
#define FAN_SPEED           0x03

#define LEFT_JUSTIFY              0x01
#define PREFIX_SIGN               0x02
#define PREFIX_BLANK              0x04
#define COMMA_TYPE                0x08
#define LONG_TYPE                 0x10
#define PREFIX_ZERO               0x20

#define CHARACTER_NUMBER_FOR_VALUE 30

extern VOID InitString(EFI_HII_HANDLE HiiHandle, STRING_REF StrRef, CHAR16 *sFormat, ...);

//-------------------------------------------------------------------------
// internal funtion declare; these funtions are only used by this file.
//-------------------------------------------------------------------------
void AdjustString(
    IN OUT  CHAR16  * Buffer,
    IN      CHAR16  * StringToChged,
    IN      UINT8   STCLen,
    IN      CHAR16  * HeadBuf,
    IN      UINT8   HeadLen,
    IN      BOOLEAN Flag,
    IN      UINT8   MidPos,
    IN      CHAR16  * TailBuf,
    IN      UINT8   TailLen
);

UINTN
HHMEfiValueToString (
    IN  OUT CHAR16  *Buffer,
    IN  INT64       Value,
    IN  UINTN       Flags,
    IN  UINTN       Width
);

void HHMCommon(
    IN      UINTN   RegData,
    IN      UINT8   Func,
    IN      UINT16  StrToken,
    IN      UINT8   RefValue,
    EFI_HII_HANDLE    hiiHandle
);
//<AMI_PHDR_START>
//----------------------------------------------------------------------
// Procedure:  HHMEfiValueToString
//
// Description:
//  This function Adjust the String shows in setup screen
//
// Input:
//  INT64       Value   ->Value need change to string
//  UINTN       Flags   ->Comma type
//  UINTN       Width   ->Character number for value
//  CHAR16      *Buffer ->Temp string for this change
// Output:
//
// OPTIONAL:
//  None
//
// Returns:
//  Index
//
//----------------------------------------------------------------------
//<AMI_PHDR_END>
UINTN
HHMEfiValueToString (
    IN  OUT CHAR16  *Buffer,
    IN  INT64       Value,
    IN  UINTN       Flags,
    IN  UINTN       Width
) {
    CHAR16      TempBuffer[CHARACTER_NUMBER_FOR_VALUE];
    CHAR16      *TempStr;
    CHAR16      *BufferPtr;
    UINTN       Count;
    UINTN       ValueCharNum;
    UINTN       Remainder;
    CHAR16      Prefix;
    UINTN       Index;
    BOOLEAN     ValueIsNegative;

    TempStr           = TempBuffer;
    BufferPtr         = Buffer;
    Count             = 0;
    ValueCharNum      = 0;
    ValueIsNegative   = FALSE;

    if (Width > CHARACTER_NUMBER_FOR_VALUE - 1) {
        Width = CHARACTER_NUMBER_FOR_VALUE - 1;
    }

    if (Value < 0) {
        *(TempStr++) = '-';
        Value        = -Value;
        ValueCharNum++;
        Count++;
        ValueIsNegative = TRUE;
    }
    do {
        if ((Width != 0) && (Count >= Width)) break;

        Value = (UINT64)Div64 ((UINT64)Value, 10, &Remainder);
        *(TempStr++) = (CHAR16)(Remainder + '0');
        ValueCharNum++;
        Count++;

        if ((Flags & COMMA_TYPE) == COMMA_TYPE) {
            if (Value != 0) {
                if ((ValueIsNegative && (ValueCharNum % 3 == 1)) || \
                    ((!ValueIsNegative) && (ValueCharNum % 3 == 0))) {
                    *(TempStr++) = ',';
                    Count++;
                }
            }
        }
    } while (Value != 0);

    if (Flags & PREFIX_ZERO) Prefix = '0';
    else Prefix = ' ';

    Index = Count;

    if (!(Flags & LEFT_JUSTIFY)) {
        for (; Index < Width; Index++)
            *(TempStr++) = Prefix;
    }
    //
    // Reverse temp string into Buffer.
    //
    if (Width == 0) {
        while (TempStr != TempBuffer)
            *(BufferPtr++) = *(--TempStr);
    }
    else {
        Index = 0;
        while ((TempStr != TempBuffer) && (Index++ < Width))
        *(BufferPtr++) = *(--TempStr);
    }
    *BufferPtr = 0;

    return Index;
}


//<AMI_PHDR_START>
//-------------------------------------------------------------------------
// Procedure:    AdjustString
//
// Description:
//  Adjust string with float point
//
// Input:
//  Buffer  -> defautl string.  ": N/A    ". Buffer length should more than 0x10
//  StringToChged -> Raw data
//  STCLen  -> String total length
//  HeadBuf -> Header of string
//  HeadLen -> Header length
//  Flag    -> TRUE indicate is a float data, False indicate it's a integet data.
//  MidPos  -> Float point position. eg: 0.076 is 0x3
//  TailBuf -> unit of string data
//  TailLen -> Length of unit
//
// Output:       Return string in Buffer
//
//-------------------------------------------------------------------------
//<AMI_PHDR_END>
VOID AdjustString(
    IN OUT  CHAR16  * Buffer,
    IN      CHAR16  * StringToChged,
    IN      UINT8   STCLen,
    IN      CHAR16  * HeadBuf,
    IN      UINT8   HeadLen,
    IN      BOOLEAN Flag,
    IN      UINT8   MidPos,
    IN      CHAR16  * TailBuf,
    IN      UINT8   TailLen
) {
    CHAR16     *temp        = Buffer;
    CHAR16     *TempSrc     = StringToChged;
    CHAR16     dod[]        ={L"."};
    CHAR16     Zero[]       ={L"0"};

    pBS->SetMem(temp, STR_BUFFER_LENGTH * sizeof(CHAR16), 0);

    if(HeadLen) {
        //Add the leading string
        pBS->CopyMem(temp, HeadBuf, (HeadLen * sizeof(CHAR16)));
        temp +=  HeadLen;
    }
    if (!Flag) {
        //Add the float point->L"."
        pBS->CopyMem(temp, TempSrc, (STCLen * sizeof(CHAR16))); //Add the string before float point
        temp += STCLen; TempSrc += STCLen;
        goto not_float_data;
    }

    if(STCLen <= MidPos) {
        //make up with a zero
        pBS->CopyMem(temp, Zero, (0x01 * sizeof(CHAR16)));      //Copy a 0
        temp++;
    } else {
        pBS->CopyMem(temp, TempSrc, ((STCLen - MidPos) * sizeof(CHAR16))); //Add the string before float point
        temp += (STCLen - MidPos); TempSrc += (STCLen - MidPos);
    }
    pBS->CopyMem(temp, dod, 0x01 * sizeof(CHAR16));             //Add the float point->L"."
    temp++;
    if(STCLen < MidPos) {
        //make up with a zero
        pBS->CopyMem(temp, Zero, ((MidPos - STCLen) * sizeof(CHAR16)));//Copy a 0
        pBS->CopyMem(temp, TempSrc, ((STCLen) * sizeof(CHAR16)));    //Add the string after float point
        temp += MidPos; TempSrc += MidPos;
    } else {
        pBS->CopyMem(temp, TempSrc, ((MidPos) * sizeof(CHAR16)));    //Add the string after float point
        temp += MidPos; TempSrc += MidPos;
    }
    not_float_data:
    if (TailLen) {
        //Add the unit
        pBS->CopyMem(temp, TailBuf, (TailLen * sizeof(CHAR16)));
    }
    return;
}


//<AMI_PHDR_START>
//-------------------------------------------------------------------------
// Procedure:    HHMCommon
//
// Description:
//  Update option with the data read from register
//
// Input:
//  RegData     -> data from SIO registers
//  Func        -> Fan Speed,Voltage and Temperature
//  StrToken    -> String token
//  OddPos      -> Odd position
//  hiiHandle
//  gblHii
//
// Output:
//  Return string in Buffer
//
//-------------------------------------------------------------------------
//<AMI_PHDR_END>
VOID HHMCommon(
    IN      UINTN     RegData,
    IN      UINT8     Func,
    IN      UINT16    StrToken,
    IN      UINT8     OddPos,
    IN      EFI_HII_HANDLE    hiiHandle
) {
    CHAR16          TailRound[]     = L" RPM";          //Fan uint
    CHAR16          TailVoltg[]     = L" V";            //Voltage uint
    CHAR16          TailTempt[]     = L" C";            //Temperature uint
    CHAR16          LeadingMini[]   = L": -";           //Fix
    CHAR16          LeadingPlus[]   = L": +";           //Fix
    CHAR16          LeadingSpac[]   = L": ";            //Fix
    CHAR16          AllSpace[]      = L": N/A       ";  //Fix
    CHAR16          *TempStr        = AllSpace;
    UINT8           StrLen;
    UINT64          NData;
    CHAR16          StrUp[STR_BUFFER_LENGTH] = L": N/A     "; //Don't change this line
    CHAR16          *StrUpdated = StrUp;
//    BOOLEAN         Flag            = 0;

    TailTempt[1] = 0x2103;
    NData = (UINT64)(RegData);
    //if temperature is negative,8 bit
//    if((Func == TEMPERATURE) && (RegData & 0x80 )) {
//    	Flag =1;
//    	NData=(UINT64)(UINT8)(~RegData+1);
//    }
    //The following may be ported by each SIO
    //As now, it support max length is five
    if(NData>9999)      StrLen = 0x5;
    else if(NData>999)  StrLen = 0x4;
    else if(NData>99)   StrLen = 0x3;
    else if(NData>9)    StrLen = 0x2;
    else                StrLen = 0x1;

    //When device not present, update to 'N/A'
    if(NData == 0x00) StrUpdated = StrUp;
    else {
        HHMEfiValueToString(TempStr, NData, 0, StrLen);
        switch(Func) {
            case    VOLTAGE:                       //Indicate it's voltage
                AdjustString(StrUpdated, TempStr, StrLen, LeadingPlus, 0x03,\
                            OddPos?TRUE:FALSE, OddPos, TailVoltg, 0x02);
                break;
            case    TEMPERATURE:                   //Indicate it's Temperature
//            	  if(!Flag) {
                AdjustString(StrUpdated, TempStr, StrLen, LeadingPlus, 0x03,\
                            OddPos?TRUE:FALSE, OddPos, TailTempt, 0x02);
//                }else{
//                AdjustString(StrUpdated, TempStr, StrLen, LeadingMini, 0x03,\
//                            OddPos?TRUE:FALSE, OddPos, TailTempt, 0x02);
//                }
                break;
            case    FAN_SPEED:                     //Indicate it's fan
                AdjustString(StrUpdated, TempStr, StrLen, LeadingSpac, 0x02,\
                            OddPos?TRUE:FALSE, OddPos, TailRound, 0x04);
                break;
            default :                              //Default to " N/A "
                break;
        }
    }
    InitString(hiiHandle, StrToken, StrUpdated);

    return;
}

//*************************************************************************
//*************************************************************************
//**                                                                     **
//**        (C)Copyright 1985-2008, American Megatrends, Inc.            **
//**                                                                     **
//**                       All Rights Reserved.                          **
//**                                                                     **
//**      5555 Oakbrook Parkway, Suite 200, Norcross, GA 30093           **
//**                                                                     **
//**                       Phone: (770)-246-8600                         **
//**                                                                     **
//*************************************************************************
//*************************************************************************

