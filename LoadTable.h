#ifndef __LOAD_TABLE__
#define  __LOAD_TABLE__

#include <Token.h>
#include <AmiDxeLib.h>
#if PI_SPECIFICATION_VERSION>=0x00010000
    #include "Protocol/FirmwareVolume2.h"
#else
    #include "Protocol/FirmwareVolume.h"
#endif

#include "Protocol/IioUds.h"

#if DYNAMIC_PCIEXBASE_SUPPORT
#include "DynamicPciBase.h"
#endif

#include "dtbb.h"
#include "OemDtbb.h"
#include "snb.h"
#include "crc32.h"

enum HwmInitDataWattIndex {
	HwmInitData60W = 0,
	HwmInitData70W,
	HwmInitData80W,
	HwmInitData95W,
	HwmInitData115W,
	HwmInitData130W,
	HwmInitData135W,
	HwmInitData150W
};

typedef enum HwmInitDataWattIndex HWM_INIT_DATA_WATT_INDEX;

#ifndef HWM_INITDATA_GUID
// d6748710-befe-4456-b6bd-dce05494ac18
#define HWM_INITDATA_GUID           { 0xd6748710, 0xbefe, 0x4456, \
  { 0xb6, 0xbd, 0xdc, 0xe0, 0x54, 0x94, 0xac, 0x18 } }
#endif


#ifndef HWM_INITDATA_60W_GUID
// d6748710-befe-4456-b6bd-dce05494ac18
#define HWM_INITDATA_60W_GUID           { 0xd6748710, 0xbefe, 0x4456, \
  { 0xb6, 0xbd, 0xdc, 0xe0, 0x54, 0x94, 0xac, 0x18 } }
#endif


#ifndef HWM_INITDATA_70W_GUID
// d6748710-befe-4456-b6bd-dce05494ac19
#define HWM_INITDATA_70W_GUID           { 0xd6748710, 0xbefe, 0x4456, \
  { 0xb6, 0xbd, 0xdc, 0xe0, 0x54, 0x94, 0xac, 0x19 } }
#endif

#ifndef HWM_INITDATA_80W_GUID
// d6748710-befe-4456-b6bd-dce05494ac1a
#define HWM_INITDATA_80W_GUID           { 0xd6748710, 0xbefe, 0x4456, \
  { 0xb6, 0xbd, 0xdc, 0xe0, 0x54, 0x94, 0xac, 0x1A } }
#endif

#ifndef HWM_INITDATA_95W_GUID
// d6748710-befe-4456-b6bd-dce05494ac1b
#define HWM_INITDATA_95W_GUID           { 0xd6748710, 0xbefe, 0x4456, \
  { 0xb6, 0xbd, 0xdc, 0xe0, 0x54, 0x94, 0xac, 0x1b } }
#endif

#ifndef HWM_INITDATA_115W_GUID
// d6748710-befe-4456-b6bd-dce05494ac1c
#define HWM_INITDATA_115W_GUID           { 0xd6748710, 0xbefe, 0x4456, \
  { 0xb6, 0xbd, 0xdc, 0xe0, 0x54, 0x94, 0xac, 0x1c } }
#endif

#ifndef HWM_INITDATA_130W_GUID
// d6748710-befe-4456-b6bd-dce05494ac1d
#define HWM_INITDATA_130W_GUID           { 0xd6748710, 0xbefe, 0x4456, \
  { 0xb6, 0xbd, 0xdc, 0xe0, 0x54, 0x94, 0xac, 0x1d } }
#endif

#ifndef HWM_INITDATA_135W_GUID
// d6748710-befe-4456-b6bd-dce05494ac1e
#define HWM_INITDATA_135W_GUID           { 0xd6748710, 0xbefe, 0x4456, \
  { 0xb6, 0xbd, 0xdc, 0xe0, 0x54, 0x94, 0xac, 0x1e } }
#endif

#ifndef HWM_INITDATA_150W_GUID
// d6748710-befe-4456-b6bd-dce05494ac1f
#define HWM_INITDATA_150W_GUID           { 0xd6748710, 0xbefe, 0x4456, \
  { 0xb6, 0xbd, 0xdc, 0xe0, 0x54, 0x94, 0xac, 0x1f } }
#endif


//a09f9438-81b5-4f5d-a39b-bed5f72c7163
#define HWM_INIT_DATA_PROTOCOL_GUID 	{0xa09f9438, 0x81b5, 0x4f5d, \
  { 0xa3, 0x9b, 0xbe, 0xd5, 0xf7, 0x2c, 0x71, 0x63 } }

static EFI_GUID gHwmInitDataGuid = HWM_INITDATA_GUID;

typedef struct _LOAD_TABLE_CONTEXT {
	HWM_INIT_DATA_WATT_INDEX	WattIndex;
	UINT32						Crc32Value;
} LOAD_TABLE_CONTEXT;


EFI_STATUS LoadTableBin(
   UINT8 **Buffer,
   UINTN *BufferSize,
   LOAD_TABLE_CONTEXT	*Ctx
);


EFI_STATUS InitData(VOID);

#ifndef _INIT_DATA_BLOCK_SIZE
#define _INIT_DATA_BLOCK_SIZE		(sizeof(union data))
#endif

#ifndef INIT_DATA_SIZE
	#define INIT_DATA_SIZE		(_INIT_DATA_BLOCK_SIZE * (5*(SIO_EC_EACH_BANKS) + 5))
#endif


#endif

