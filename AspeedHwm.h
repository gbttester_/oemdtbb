#ifndef __ASPEED_HWM_H__
#define __ASPEED_HWM_H__

#include <Token.h>
#include <AmiDxeLib.h>
#if PI_SPECIFICATION_VERSION>=0x00010000
    #include "Protocol/FirmwareVolume2.h"
#else
    #include "Protocol/FirmwareVolume.h"
#endif

#if DYNAMIC_PCIEXBASE_SUPPORT
#include "DynamicPciBase.h"
#endif

#include "dtbb.h"
#include "OemDtbb.h"
#include "LoadTable.h"


#ifndef _INIT_DATA_BLOCK_SIZE
#define _INIT_DATA_BLOCK_SIZE		(sizeof(union data))
#endif

#ifndef NR_CTRL_PER_BANK
    #define NR_CTRL_PER_BANK				(10)
#endif

#ifndef _HWM_GET_VALUE

#if !defined(EFI_DEBUG) || !defined(DTBB_TRACE)
	#define __MY_TRACE
#else
	#define __MY_TRACE		TRACE
#endif

#define _HWM_GET_VALUE(_bf, _name, _bank, _ctrl_idx)		\
	( __MY_TRACE((-1, "Ref(%08x)\n", _TO_AHB_ADDR(&((_bf)->_name[(_bank)].un.ec.io_buff[(_ctrl_idx)])))) \
	 , ((_bf)->_name[(_bank)].un.ec.io_buff[(_ctrl_idx)])\
	)
#endif

#endif

