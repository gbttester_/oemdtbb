#include <AmiDxeLib.h>
#include "LoadTable.h"
#include "AspeedHwm.h"

// #include "Board\Cpu\PlatformCpuLib.h"
#include "Setup.h"

#ifndef DEBUG_FUNC
#define DEBUG_FUNC      TRACE
#endif

// static
// SETUP_CPU_FEATURES gSetupCpuFeatures;

static EFI_GUID gInitDataGuid[] = {
    HWM_INITDATA_60W_GUID,
    HWM_INITDATA_70W_GUID,
    HWM_INITDATA_80W_GUID,
    HWM_INITDATA_95W_GUID,
    HWM_INITDATA_115W_GUID,
    HWM_INITDATA_130W_GUID,
    HWM_INITDATA_135W_GUID,
    HWM_INITDATA_150W_GUID
};

static
HWM_INIT_DATA_WATT_INDEX GetInitDataIndex(
   UINT32 Tdp
)
{
    switch (Tdp) {
        case 60:
            return HwmInitData60W;
        case 70:
            return HwmInitData70W;
        case 80:
            return HwmInitData80W;
        case 95:
            return HwmInitData95W;
        case 115:
            return HwmInitData115W;
        case 130:
            return HwmInitData130W;
        case 135:
            return HwmInitData135W;
        case 150:
            return HwmInitData150W;

    default:
        return HwmInitData150W;
    }
}

static
EFI_GUID* GetInitDataGuid(
   UINT32 Tdp
)
{
    HWM_INIT_DATA_WATT_INDEX index;

    index = GetInitDataIndex(Tdp);
    return &gInitDataGuid[index];
}

static UINT8 mInitDataBuf[INIT_DATA_SIZE];

void __inline hexdump(UINT8 *buf, UINT32 length) {
    UINTN i;

    for (i = 0; i != length; i++) {
        DEBUG_FUNC((0xffffffff, "%02x ", buf[i]));
        if (i != 0 && (i + 1) % 16 == 0) {
            DEBUG_FUNC((0xffffffff, "\n"));
        }
    }
    if (!(i != 0 && (i + 1) % 16 == 0)) {
        DEBUG_FUNC((0xffffffff, "\n"));
    }
}

UINT8 IsSkt1Present(VOID)
{
    EFI_STATUS              Status = EFI_SUCCESS;
    EFI_IIO_UDS_PROTOCOL    *IioUdsProto = NULL;

    Status = pBS->LocateProtocol(&gEfiIioUdsProtocolGuid, NULL, &IioUdsProto);

    if (EFI_ERROR(Status)) {
        return TRUE;
    }

    return IioUdsProto->IioUdsPtr->SystemStatus.numCpus != 1;
}

UINT8 IsInitDataValid(
   UINT8 *Buffer,
   UINTN BufferSize
)
{
    UINTN i;

    for (i = 0; i != BufferSize; i++) {
        if (Buffer[i] != 0) {
            return TRUE;
        }
    }
    return FALSE;
}

EFI_STATUS LoadTableBin(
   UINT8 **Buffer,
   UINTN *BufferSize,
   LOAD_TABLE_CONTEXT   *Ctx)
{
    EFI_STATUS                    Status;
    EFI_HANDLE                    *HandleBuffer;
    UINTN                         NumberOfHandles;
    EFI_FV_FILETYPE               FileType;
    UINT32                        FvStatus;
    EFI_FV_FILE_ATTRIBUTES        Attributes;
    UINTN                         Size;
    UINTN                         i;
#if PI_SPECIFICATION_VERSION>=0x00010000
    EFI_FIRMWARE_VOLUME2_PROTOCOL *FwVol;
#else
    EFI_FIRMWARE_VOLUME_PROTOCOL  *FwVol;
#endif
    EFI_GUID                      *TargetFfsGuid = NULL;
    UINT32                        PkgTdp = 0;
    UINT8                         Failback = FALSE;

    Status = 0;

    if (Buffer == NULL) {
        return EFI_INVALID_PARAMETER;
    }
    if (BufferSize == NULL) {
        return EFI_INVALID_PARAMETER;
    }

    HandleBuffer = NULL;
    FwVol = NULL;
    Status = pBS->LocateHandleBuffer(
       ByProtocol,
#if PI_SPECIFICATION_VERSION>=0x00010000
       &gEfiFirmwareVolume2ProtocolGuid,
#else
       &gEfiFirmwareVolumeProtocolGuid,
#endif
       NULL,
       &NumberOfHandles,
       &HandleBuffer
       );
    ASSERT_EFI_ERROR(Status);

    PkgTdp = read_tdp(0);
    TargetFfsGuid = GetInitDataGuid(PkgTdp);

    TRACE((-1, "PkgTdp: %x, TargetFfsGuid:%g\n", PkgTdp, TargetFfsGuid));

    for (i = 0; i < NumberOfHandles; i++) {
        Status = pBS->HandleProtocol(
           HandleBuffer[i],
#if PI_SPECIFICATION_VERSION>=0x00010000
           &gEfiFirmwareVolume2ProtocolGuid,
#else
           &gEfiFirmwareVolumeProtocolGuid,
#endif
           &FwVol
           );
        ASSERT_EFI_ERROR(Status);
        #define TRACE_LOC   TRACE((-1, "%s @ %d\n", __FUNCTION__, __LINE__));

        Size      = 0;
        FvStatus  = 0;
        Status = FwVol->ReadFile(
           FwVol,
           TargetFfsGuid,
           NULL,
           &Size,
           &FileType,
           &Attributes,
           &FvStatus
           );

        if (Status == EFI_SUCCESS) {
            if (*BufferSize < Size) {
                TRACE((-1, "Size:%x BufferSize:%x\n", Size, *BufferSize));
                Status = EFI_INVALID_PARAMETER;
                goto OnError;
            }
            Status = FwVol->ReadFile(
               FwVol,
               TargetFfsGuid,
               Buffer,
               &Size,
               &FileType,
               &Attributes,
               &FvStatus
               );
            DEBUG_FUNC((-1, "ReadFile, Status:%x\n", Status));
            if (!EFI_ERROR(Status)) {
                if (IsInitDataValid(*Buffer, Size)) {
                    DEBUG_FUNC((-1, "Data Valid\n"));
                    *BufferSize = Size;
                    if (Ctx != NULL) {
                        Ctx->WattIndex = GetInitDataIndex(PkgTdp);
                        Ctx->Crc32Value = Crc32_ComputeBuf(0, *Buffer, (UINT32)Size);
                        TRACE((-1, "WattIndex:%x Crc32Value:%x\n", Ctx->WattIndex, Ctx->Crc32Value));
                    }
                    break;
                } else {
                    DEBUG_FUNC((-1, "Data not Valid\n"));
                    Failback = TRUE;
                    break;
                }
            }
        }
    }


    if (Failback == TRUE) {
        for (i = 0; i < NumberOfHandles; i++) {
            Status = pBS->HandleProtocol(
               HandleBuffer[i],
    #if PI_SPECIFICATION_VERSION>=0x00010000
               &gEfiFirmwareVolume2ProtocolGuid,
    #else
               &gEfiFirmwareVolumeProtocolGuid,
    #endif
               &FwVol
               );
            ASSERT_EFI_ERROR(Status);

            for (i = 0; i != HwmInitData150W; i++) {
                TargetFfsGuid = &gInitDataGuid[HwmInitData150W - i];
                Size      = 0;
                FvStatus  = 0;
                Status = FwVol->ReadFile(
                       FwVol,
                       TargetFfsGuid,
                       NULL,
                       &Size,
                       &FileType,
                       &Attributes,
                       &FvStatus
                   );

                if (Status == EFI_SUCCESS) {
                    if (*BufferSize < Size) {
                        Status = EFI_INVALID_PARAMETER;
                        goto OnError;
                    }
                    Status = FwVol->ReadFile(
                           FwVol,
                           TargetFfsGuid,
                           Buffer,
                           &Size,
                           &FileType,
                           &Attributes,
                           &FvStatus
                       );
                    DEBUG_FUNC((-1, "ReadFile, Status:%x\n", Status));
                    if (!EFI_ERROR(Status)) {
                        if (IsInitDataValid(*Buffer, Size)) {
                            *BufferSize = Size;
                            if (Ctx != NULL) {
                                Ctx->WattIndex = GetInitDataIndex(PkgTdp);
                                Ctx->Crc32Value = Crc32_ComputeBuf(0, *Buffer, (UINT32)Size);
                                TRACE((-1, "WattIndex:%x Crc32:%x\n", Ctx->WattIndex, Ctx->Crc32Value));
                            }
                            break;
                        } else {
                            Status = EFI_CRC_ERROR;
                        }
                    }
                }
            }
            if (Status == EFI_INVALID_PARAMETER || Status == EFI_SUCCESS) {
                break;
            }
        }
    }

OnError:
    if (HandleBuffer != NULL) {
        pBS->FreePool(HandleBuffer);
    }

    return Status;
}

EFI_STATUS LoadTableBin2(
   UINT8 **Buffer,
   UINTN *BufferSize
   )
{
    EFI_STATUS                    Status;
    EFI_HANDLE                    *HandleBuffer;
    UINTN                         NumberOfHandles;
    EFI_FV_FILETYPE               FileType;
    UINT32                        FvStatus;
    EFI_FV_FILE_ATTRIBUTES        Attributes;
    UINTN                         Size;
    UINTN                         i;
#if PI_SPECIFICATION_VERSION>=0x00010000
    EFI_FIRMWARE_VOLUME2_PROTOCOL *FwVol;
#else
    EFI_FIRMWARE_VOLUME_PROTOCOL  *FwVol;
#endif

    Status = 0;

    if (Buffer == NULL) {
        return EFI_INVALID_PARAMETER;
    }
    if (BufferSize == NULL) {
        return EFI_INVALID_PARAMETER;
    }

    FwVol = NULL;
    Status = pBS->LocateHandleBuffer(
       ByProtocol,
#if PI_SPECIFICATION_VERSION>=0x00010000
       &gEfiFirmwareVolume2ProtocolGuid,
#else
       &gEfiFirmwareVolumeProtocolGuid,
#endif
       NULL,
       &NumberOfHandles,
       &HandleBuffer
       );
    ASSERT_EFI_ERROR(Status);

    for (i = 0; i < NumberOfHandles; i++) {
        Status = pBS->HandleProtocol(
           HandleBuffer[i],
#if PI_SPECIFICATION_VERSION>=0x00010000
           &gEfiFirmwareVolume2ProtocolGuid,
#else
           &gEfiFirmwareVolumeProtocolGuid,
#endif
           &FwVol
           );
        ASSERT_EFI_ERROR(Status);

        Size      = 0;
        FvStatus  = 0;
        Status = FwVol->ReadFile(
           FwVol,
           &gHwmInitDataGuid,
           NULL,
           &Size,
           &FileType,
           &Attributes,
           &FvStatus
           );

        if (Status == EFI_SUCCESS) {
            if (*BufferSize < Size) {
                return EFI_INVALID_PARAMETER;
            }
            Status = FwVol->ReadFile(
               FwVol,
               &gHwmInitDataGuid,
               Buffer,
               &Size,
               &FileType,
               &Attributes,
               &FvStatus
               );
            DEBUG_FUNC((-1, "ReadFile, Status:%x\n", Status));
            if (!EFI_ERROR(Status)) {
                *BufferSize = Size;
                break;
            }
            break;
        }
    }

    return Status;
}


void TraceMemCpy(VOID *Dst, VOID *Src, UINTN Size)
{
    TRACE((-1, "MemCopy(%x, %x, %x) Dst:(%x)\n", Dst, Src, Size, _TO_AHB_ADDR(Dst)));

    hexdump(Src, (UINT32)Size);
    pBS->CopyMem(Dst, Src, Size);
}

#define _DUMP_MEM(_bf, _mem, _ele_size, _buf_size) \
do {\
    {\
        UINT32 i;\
        TRACE((-1, "Dump:(%x)\n", _TO_AHB_ADDR(&((_bf)->_mem[0].un))));\
        for (i = 0; i != (_ele_size); i++) {\
            hexdump((VOID*)&((_bf)->_mem[i].un), (_buf_size));\
        }\
    }\
}\
while (FALSE);

#define _DRY_RUN        1

#if !defined(_MEM_CPY) && _DRY_RUN
#define _MEM_CPY(_dst, _src, _size)\
    TraceMemCpy(_dst, _src, _size);
#endif

#if !defined(_MEM_CPY) && !_DRY_RUN
#define _MEM_CPY(_dst, _src, _size) \
    pBS->CopyMem(_dst, _src, _size)
#endif

#ifndef _FILL_INIT_DATA
#define _FILL_INIT_DATA(_bf, _mem, _ele_size, _buf_size, _src) \
do {\
    {\
        UINT32 i;\
        UINT8 *p = (UINT8*)&(_src);\
        for (i = 0; i != (_ele_size); i++) {\
            _MEM_CPY(&((_bf)->_mem[i].un), &p[(_buf_size) * i], (_buf_size));\
        }\
    }\
}\
while (FALSE);

#endif

// !!! Rex required Begin !!!
#if 1

#if !defined(_ZERO_IO_ITEM_LIST)
#define _ZERO_IO_ITEM_LIST          { 21, 26, 27, 28, 29 } // To match your FSC init data table
#endif

EFI_STATUS OemTableHook(bios_form *bf)
{
    EFI_STATUS    Status     = 0;
    UINTN         ioItemNum;
    UINTN         Idx;
    UINTN         bank       = 0;
    UINTN         ioIdx      = 0;
    static UINT8  ZeroList[] = _ZERO_IO_ITEM_LIST;

    if (IsSkt1Present()) {
        return EFI_SUCCESS;
    }

    for (Idx = 0; Idx != sizeof(ZeroList)/sizeof(UINT8); Idx++) {
        ioItemNum = ZeroList[Idx];
        bank = ioItemNum / NR_CTRL_PER_BANK;
        ioIdx = ioItemNum % NR_CTRL_PER_BANK;
        TRACE((-1, "bank:%x Idx:%x\n", bank, ioIdx));
        _ZERO_IO_ITEM(bf, sio_temp,  bank,  ioIdx);
    }

    return Status;
}
#endif
// !!! Rex required End !!!

EFI_STATUS InitData(VOID)
{
    EFI_STATUS  Status;
    UINTN       BufferSize;
    UINT8       *Data;
    UINT32      i;
    bios_form   *bf;
    static LOAD_TABLE_CONTEXT   ctx = {0};
    static EFI_GUID     gHwmInitDataProtoGuid = HWM_INIT_DATA_PROTOCOL_GUID;
    EFI_HANDLE  Handle = NULL;

    bf = NULL;
    BufferSize = 0;
    Data = NULL;
    i = 0;
    Data = &mInitDataBuf[0];
    BufferSize = INIT_DATA_SIZE;
    Status = LoadTableBin(&Data, &BufferSize, &ctx);

    DEBUG_FUNC((-1, "InitDataSize:%x\n", BufferSize));
    if (!EFI_ERROR(Status)) {
        if (BufferSize > INIT_DATA_SIZE) {
            return EFI_BUFFER_TOO_SMALL;
        }
    } else {
        DEBUG_FUNC((-1, "Cant Get Init Data\n"));
        return EFI_NOT_READY;
    }
    Status = pBS->InstallMultipleProtocolInterfaces(&Handle,
                                                    &gHwmInitDataProtoGuid,
                                                    &ctx,
                                                    NULL,
                                                    NULL);
    DEBUG_FUNC((-1, "Install HwmInitDataProto:%x\n", Status));

    Status = OemDtbbInit();
    TRACE((-1, "mSharedAhbAddr:%x, size:%x\n", mSharedAhbAddr, mSharedAhbSize));
    ASSERT_EFI_ERROR(Status);

    Status = MapShareRegion((UINT32)mSharedAhbAddr, (UINT32*)&bf);
    if (EFI_ERROR(Status)) {
        TRACE((-1, "Cant Map share region, Status:%x\n", Status));
    } else {
        TRACE((-1, "Map share region okey!, bf:%x\n", bf));
    }
    TRACE((-1, "Zero Mem\n"));
    pBS->SetMem(bf, sizeof(bios_form), 0);

    // Copy init data into AHB space
    TRACE((-1, "Fill Init Data\n"));

    _FILL_INIT_DATA(bf, sio_vin, SIO_EC_EACH_BANKS, sizeof(ec_t), Data[0]);
    _FILL_INIT_DATA(bf, sio_temp, SIO_EC_EACH_BANKS, sizeof(ec_t), Data[SIO_EC_EACH_BANKS * sizeof(ec_t)]);
    _FILL_INIT_DATA(bf, sio_fan, SIO_EC_EACH_BANKS, sizeof(ec_t), Data[SIO_EC_EACH_BANKS * 3 * sizeof(ec_t)]);
    _FILL_INIT_DATA(bf, sio_pwm, SIO_EC_EACH_BANKS, sizeof(ec_t), Data[SIO_EC_EACH_BANKS * 2 * sizeof(ec_t)]);
    _FILL_INIT_DATA(bf, i2cdev_init, SIO_EC_EACH_BANKS, sizeof(ec_t), Data[SIO_EC_EACH_BANKS * 4 * sizeof(ec_t)]);
    _FILL_INIT_DATA(bf, sio_fsc, 5, sizeof(fsc_sm_t), Data[SIO_EC_EACH_BANKS * 5 * sizeof(ec_t)]);
    TRACE((-1, "Fill Init Data done\n"));

    Status = OemTableHook(bf);

    // inform BMC to parse init data
    _FILL_ACTION(bf, sio_vin,   SIO_EC_EACH_BANKS,  SM_ACTION_INIT);
    _FILL_ACTION(bf, sio_temp,  SIO_EC_EACH_BANKS,  SM_ACTION_INIT);
    _FILL_ACTION(bf, sio_fan,   SIO_EC_EACH_BANKS,  SM_ACTION_INIT);

    // Don't setup PWM output
    _FILL_ACTION(bf, sio_pwm,   SIO_EC_EACH_BANKS,  SM_ACTION_INIT);
    _FILL_ACTION(bf, i2cdev_init, SIO_EC_EACH_BANKS,  SM_ACTION_INIT);
    _FILL_ACTION(bf, sio_fsc, 5, SM_ACTION_INIT);
    TRACE((-1, "Fill init Action done\n"));
    // Wait for 20 secs
    #if 0
    {
        UINT32 i;
        TRACE((-1, "Waiting for 20 sec: "));
        for (i = 0; i != 20; i++) {
            TRACE((-1, "."));
            pBS->Stall(1 * 1000 * 1000);
        }
        TRACE((-1, "\n"));
    }
    #endif
    // TRACE((-1, "Fill get Action done\n"));
    //_FILL_ACTION(bf, sio_vin, SIO_EC_EACH_BANKS, SM_ACTION_GET);
    //_FILL_ACTION(bf, sio_temp, SIO_EC_EACH_BANKS, SM_ACTION_GET);
    //_FILL_ACTION(bf, sio_fan, SIO_EC_EACH_BANKS, SM_ACTION_GET);

    return Status;
}
