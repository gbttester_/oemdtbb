#include <AmiDxeLib.h>
#include "OemDtbb.h"

#include "Protocol/PciIo.h"

#include "dtbb.h"

// #pragma optimize( "", off )

EFI_STATUS test_version(void)
{
	EFI_STATUS				Status;
	volatile bios_form		* volatile bf;
	UINT32			Sdr;
	UINT32			Fru;
	UINT32			Fw;

	TRACE((-1, "SharedAddr:%x\n", mSharedAhbAddr));

	Status = MapShareRegion((UINT32)mSharedAhbAddr, (UINT32*)&bf);
	if (EFI_ERROR(Status)) {
		TRACE((-1, "Cant Map share region, Status:%x\n", Status));
	} else {
		TRACE((-1, "Map share region okey!"));
	}

	TRACE((-1, "bf:%x\n", bf));
	pBS->SetMem((void*)bf, sizeof(bios_form), 0);
	bf->version.action = DTBB_ACTION_GET;

	Status = DoorRingFinished(0);

	TRACE((-1, "DoorbellRing,Status:%x\n", Status));
	TRACE((-1, "OEM CC: %x\n",  bf->version.compcode));

	TRACE((-1, "addr fw_ver:%x\n", &bf->version.un.ver.fw[0]));

	Sdr = *(volatile UINT32*)&bf->version.un.ver.sdr[0];
	Fw = *(volatile UINT32*)&bf->version.un.ver.fw[0];
	Fru = *(volatile UINT32*)&bf->version.un.ver.fru[0];
	TRACE((-1, "FwVer:%x\n", Fw));
	TRACE((-1, "FruVer:%x\n", Fru));
	TRACE((-1, "SdrVer:%x\n", Sdr));

	bf->version.action = DTBB_ACTION_NULL;

	return Status;
}

EFI_STATUS test_get_bmc_net_param(void)
{
	EFI_STATUS	volatile	Status;
	volatile bios_form		* volatile bf2;
	UINT32		ip_addr;
	UINT32		gateway;
	UINT32		netmask;
	UINT8		ip_src;

	Status = MapShareRegion((UINT32)mSharedAhbAddr, (UINT32*)&bf2);
	if (EFI_ERROR(Status)) {
		TRACE((-1, "Cant Map share region, Status:%x\n", Status));
	} else {
		TRACE((-1, "Map share region okey!"));
	}

	TRACE((-1, "bf2:%x\n", bf2));
	// pBS->SetMem((void*)bf2, sizeof(bios_form), 0);
	bf2->ipv4.action = DTBB_ACTION_GET;
	bf2->ipv4.un.ip.lan_info[0] = 1;

	Status = DoorRingFinished(0);
	bf2->ipv4.action = DTBB_ACTION_NULL;


	TRACE((-1, "addr ip_addr:%x\n", &bf2->ipv4.un.ip.ip_addr));

	ip_addr = *(volatile UINT32 *)(&bf2->ipv4.un.ip.ip_addr);
	netmask = *(volatile UINT32 *)(&bf2->ipv4.un.ip.netmask);
	gateway = *(volatile UINT32 *)(&bf2->ipv4.un.ip.gateway);
	ip_src = bf2->ipv4.un.ip.lan_info[1];

	TRACE((-1, "OEM CC: %x\n", bf2->ipv4.compcode));
	TRACE((-1, "ip_src: %x\n", ip_src));
	TRACE((-1, "ip_addr:%x\n", ip_addr));
	TRACE((-1, "netmask:%x\n", netmask));
	TRACE((-1, "gateway:%x\n", gateway));

	return Status;
}

UINT32 htonl(UINT32 hostlong)
{
    return ((hostlong << 24) | ((hostlong & 0xff00) << 8) |
            ((hostlong & 0xff0000) >> 8) | (hostlong >> 24));
}

EFI_STATUS test_set_bmc_net_param(void)
{
	EFI_STATUS	volatile	Status;
	volatile bios_form		* volatile bf2;

	Status = MapShareRegion((UINT32)mSharedAhbAddr, (UINT32*)&bf2);
	if (EFI_ERROR(Status)) {
		TRACE((-1, "Cant Map share region, Status:%x\n", Status));
	} else {
		TRACE((-1, "Map share region okey!"));
	}

	TRACE((-1, "bf2:%x\n", bf2));
	// pBS->SetMem((void*)bf2, sizeof(bios_form), 0);
	bf2->ipv4.action = DTBB_ACTION_SET;
	bf2->ipv4.un.ip.lan_info[0] = 1;
	bf2->ipv4.un.ip.lan_info[1] = 2;

	bf2->ipv4.un.ip.ip_addr[0] = 10;
	bf2->ipv4.un.ip.ip_addr[1] = 10;
	bf2->ipv4.un.ip.ip_addr[2] = 10;
	bf2->ipv4.un.ip.ip_addr[3] = 10;

	bf2->ipv4.un.ip.netmask[0] = 255;
	bf2->ipv4.un.ip.netmask[1] = 255;
	bf2->ipv4.un.ip.netmask[2] = 0;
	bf2->ipv4.un.ip.netmask[3] = 0;

	bf2->ipv4.un.ip.gateway[0] = 10;
	bf2->ipv4.un.ip.gateway[1] = 1;
	bf2->ipv4.un.ip.gateway[2] = 27;
	bf2->ipv4.un.ip.gateway[3] = 253;

	Status = DoorRingFinished(0);
	bf2->ipv4.action = DTBB_ACTION_NULL;

	TRACE((-1, "DoorbellRing,Status:%x\n", Status));
	TRACE((-1, "OEM CC: %x\n", bf2->ipv4.compcode));

	return Status;
}

EFI_STATUS test_get_fru_sdr(UINT8 param_val)
{
	EFI_STATUS	volatile	Status;
	volatile bios_form		* volatile bf2;
	UINT32					FruRawLen;
	UINT32  				FruRawAddr;
	UINT32					SdrRawLen;
	UINT32					SdrRawAddr;

	Status = MapShareRegion((UINT32)mSharedAhbAddr, (UINT32*)&bf2);
	if (EFI_ERROR(Status)) {
		TRACE((-1, "Cant Map share region, Status:%x\n", Status));
	} else {
		TRACE((-1, "Map share region okey!"));
	}
	pBS->SetMem((void*)bf2, sizeof(bios_form), 0);

	TRACE((-1, "bf2:%x\n", bf2));
	// pBS->SetMem((void*)bf2, sizeof(bios_form), 0);
	bf2->req_info.action = DTBB_ACTION_GET;
	bf2->req_info.un.td.size = 0;

	bf2->req_info.un.td.value[0] = param_val;

	TRACE((-1, "value: %x\n", param_val));

	bf2->req_info.compcode = 0xffffffff;

	Status = DoorRingFinished(0);
	TRACE((-1, "DoorRingF, Status:%x\n", Status));
	// bf2->ipv4.action = DTBB_ACTION_NULL;

	//pBS->Stall(1000*20);

	TRACE((-1, "OEM CC: %x\n", bf2->req_info.compcode));
	TRACE((-1, "addr fru raw len: %x\n", &bf2->req_info.un.req.fru_len));
	TRACE((-1, "addr fru raw addr: %x\n", &bf2->req_info.un.req.fru_index));

	FruRawLen = bf2->req_info.un.req.fru_len;
	FruRawAddr = bf2->req_info.un.req.fru_index;
	SdrRawLen = bf2->req_info.un.req.sel_len;
	SdrRawAddr = bf2->req_info.un.req.sel_index;

	TRACE((-1, "FruRawLen: %x\n", FruRawLen));
	TRACE((-1, "FruRawAddr: %x\n", FruRawAddr));
	TRACE((-1, "SdrRawLen: %x\n", SdrRawLen));
	TRACE((-1, "SdrRawAddr: %x\n", SdrRawAddr));

	if (!FruRawLen) {
		TRACE((-1, "fru len is null\n"));
	}
	if (!FruRawAddr) {
		TRACE((-1, "fru addr is null\n"));
	}

	//if (param_val ==) {
	//	Status = MapShareRegion((UINT32)FruRawAddr, (UINT32*)&Mapping);
	//}

	return Status;
}


void do_all_test(void)
{
	EFI_STATUS	Status;
	TRACE((-1, "xxxx\n"));
	Status = 0;
	Status = test_version();
	// Status = test_get_fru_sdr(1);
	// Status = test_get_fru_sdr(2);
	Status = test_get_fru_sdr(3);
	// Status = test_set_bmc_net_param();
	// Status = test_get_bmc_net_param();
	//Status = test_set_bmc_net_param();
}
