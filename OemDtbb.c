#include <AmiDxeLib.h>
#include "OemDtbb.h"
#include "DtbbProtocol.h"
#include "LoadTable.h"

#if 0

EFI_GUID    gEfiIpmiTransportProtocolGuid = EFI_IPMI_TRANSPORT_PROTOCOL_GUID;

EFI_IPMI_TRANSPORT  *mIpmiTransportProtocol;
#endif


/*
 * Shared Memory Address will be static always.
 */


UINT32 mSharedAhbAddr = (UINT32)NULL;
UINT32 mSharedAhbSize = 0;
UINT32 mWindow = 0;

void __inline hexdump(UINT8 *buf, UINT32 length)
{
    UINTN i;

    for (i=0; i != length; i++) {
        DEBUG_FUNC((0xffffffff, "%02x ", buf[i]));
        if (i!=0 && (i+1)%16 == 0) {
            DEBUG_FUNC((0xffffffff, "\n"));
        }
    }
    if (!(i!=0 && (i+1)%16 == 0)) {
            DEBUG_FUNC((0xffffffff, "\n"));
    }
}

#if 1
EFI_STATUS EFIAPI MapShareRegion(
    IN UINT32 Dst,
    IN OUT UINT32 *HostAddress
)
{
    EFI_STATUS           Status       = EFI_SUCCESS;
    EFI_HANDLE          *HandleBuffer = NULL;
    UINTN                n;
    UINTN                HandleCount;
    EFI_PCI_IO_PROTOCOL *PciIo;
    UINT32              *Mmio = NULL;
    UINT32               Window;
    UINT32               DidVid;
    UINT32               SubOffset;

    // ASSERT(Dst % 0x10000 == 0);
    DidVid = 0;
    PciIo = NULL;
    // DEBUG_FUNC((0xffffffff, "Dst: %x\n", Dst));
    SubOffset = Dst & 0xFFFF;
    Dst = Dst & 0xFFFF0000;

    Status = pBS->LocateHandleBuffer (
        ByProtocol,
        &gEfiPciIoProtocolGuid,
        NULL,
        &HandleCount,
        &HandleBuffer);

    ASSERT_EFI_ERROR(Status);
    if (EFI_ERROR (Status)) {
        return Status;
    }

    for (n = 0; n < HandleCount; n++) {
        Status = pBS->HandleProtocol (HandleBuffer[n], &gEfiPciIoProtocolGuid, &PciIo);
        ASSERT_EFI_ERROR(Status);
        Status = PciIo->Pci.Read(
            PciIo,
            EfiPciIoWidthUint32,
            0,  // offset
            1,  // width
            &DidVid);

        ASSERT_EFI_ERROR(Status);
        if (DidVid == 0x20001A03) {
            //DEBUG_FUNC((0xffffffff, "Found:VidDid = %x\n", DidVid));
            break;
        }
    }

    if (DidVid != 0x20001A03) {
        //DEBUG_FUNC((0xffffffff, "Not Found\n"));
        goto OnError;
    }

    if (PciIo) {
        Status = PciIo->Pci.Read(
                PciIo,
                EfiPciIoWidthUint32,
                0x14,  // offset
                0x1,  // width
                &Mmio);
		if (EFI_ERROR(Status)) {
			DEBUG_FUNC((-1, "Can't read PciIo, Status:%x\n", Status));
			goto OnError;
		}
		// DEBUG_FUNC((0xffffffff, "Mmio = %x\n", (UINT32)Mmio));
        ASSERT_EFI_ERROR(Status);
        ((UINT32*)((UINT8*)Mmio + 0xF000))[0] = 1;
        ((UINT32*)((UINT8*)Mmio + 0xF000))[1] = Dst;
        Window = (UINT32)((UINTN)Mmio + 0x10000);
		mWindow = Window;

        // DEBUG_FUNC((0xffffffff, "Window = %x\n", Window));
        // DEBUG_FUNC((0xffffffff, "Dst: %x\n", Dst));
        // DEBUG_FUNC((0xffffffff, "SubOffset: %x\n", SubOffset));
        if (HostAddress) {
            *HostAddress = (Window + SubOffset);
        }
    }
OnError:
    if (HandleBuffer) {
        pBS->FreePool(HandleBuffer);
    }
    return Status;
}
#endif

#if 0
/**
 * @brief To get the shared region between BMC space.
 *
 * @param Address       - The address in Aspeed space.
 * @param Length        - The shared region length in Mega bytes.
 *
 */
EFI_STATUS GetSharedAddress(OUT UINT32 *Address, OUT UINT32 *Length)
{
    EFI_STATUS Status   = EFI_SUCCESS;
    UINT8      Req[3]   = {0xc0, 0x00, 0x00};
    UINT8      Resp[32];
    UINT8      RespLen = sizeof(Resp);

    if (!Address || !Length) {
        return EFI_INVALID_PARAMETER;
    }
    Status = mIpmiTransportProtocol->SendIpmiCommand(
            mIpmiTransportProtocol,
            EFI_SM_OEM_GROUP,
            0,
            CMD_GET_SHARED_REGION,
            &Req[0],
            sizeof(Req),
            &Resp[0],
            &RespLen);

    if (EFI_ERROR(Status)) {
        if(Status == EFI_DEVICE_ERROR){
            DEBUG_FUNC((0xffffffff, "CMD_GET_SHARED_REGION failed, CC = %x\n", Resp[0]));
        } else {
            DEBUG_FUNC((0xffffffff, "CMD_GET_SHARED_REGION failed, Status = %x\n", Status));
        }
        goto OnError;
    }
    *Address = *(UINT32*)&Resp[0];
    *Length = *(UINT32*)&Resp[4];
OnError:
    return Status;
}
#else

#ifndef OEM_DTBB_FIXED_ADDRESS
    #define OEM_DTBB_FIXED_ADDRESS      (0x46f00000)
#endif

#ifndef OEM_DTBB_FIXED_SIZE
    #define OEM_DTBB_FIXED_SIZE         (1024*1024)
#endif

EFI_STATUS EFIAPI GetSharedAddress(OUT UINT32 *Address, OUT UINT32 *Length)
{
    EFI_STATUS      Status = 0;

    *Address = OEM_DTBB_FIXED_ADDRESS;
    *Length = OEM_DTBB_FIXED_SIZE;

    return Status;
}

#endif

/**
 * @brief To copy specified data bytes to Shared region.
 *
 * @param Src       - The source address from Host-side
 * @param Dst       - The destination address within Shared Region, must be 4byte aligned.
 * @param Length    - The length in bytes.
 *
 * @return
 */
EFI_STATUS EFIAPI CopyToSharedRegion(
    IN UINT32 *Src,
    IN UINT32 Dst,
    IN UINT32 Length)
{
    EFI_STATUS           Status       = EFI_SUCCESS;
    UINT32              *Window;
    UINT32               i;

    ASSERT(Dst % 0x10000 == 0);
    ASSERT(Length);

    Window = NULL;

    Status = MapShareRegion(Dst, (UINT32*)&Window);
    if (EFI_ERROR(Status)) {
        DEBUG_FUNC((0xffffffff, "Can't Map Share Region, TargetAddr:%x\n", Dst));
    } else {
        DEBUG_FUNC((0xffffffff, "Window: %x\n", Window));
    }
    hexdump((UINT8*)&Src[0], Length);
    if (Window) {
        for (i = 0; i < Length;) {
            Window[i/4] = Src[i/4];
            i += 4;
        }
    }

//OnError:
    return Status;
}

/**
 * @brief To ring the doorbell to indicate the host-side finish the update to
 *        Shared space.
 *
 * @param Length        - The length in bytes.
 *
 * @return
 */
#if 0
EFI_STATUS DoorRingFinished(UINT16 Length)
{
    EFI_STATUS Status   = EFI_SUCCESS;
    UINT8      Resp[64];
    UINT8      Req[4]   = {0xc0, 0x81, 0x00, 0x00};
    UINT8      RespLen = sizeof(Resp);

    *(UINT16*)&Req[2] = Length;
    Status = mIpmiTransportProtocol->SendIpmiCommand(
            mIpmiTransportProtocol,
            EFI_SM_OEM_GROUP,
            0,
            CMD_RING_SHARED_REGION,
            &Req[0],
            sizeof(Req),
            &Resp[0],
            &RespLen);
    if (EFI_ERROR(Status)) {
        if(Status == EFI_DEVICE_ERROR){
            DEBUG_FUNC((0xffffffff, "CMD_RING_SHARED_REGION failed, CC = %x\n", Resp[0]));
        } else {
            DEBUG_FUNC((0xffffffff, "CMD_RING_SHARED_REGION failed, Status = %x\n", Status));
        }
        goto OnError;
    } else {
        DEBUG_FUNC((0xffffffff, "CMD_RING_SHARED_REGION successfully, RespLen = %x\n", RespLen));
    }
    hexdump(&Resp[0], RespLen);
OnError:
    return Status;
}
#else
EFI_STATUS EFIAPI DoorRingFinished(UINT16 Length)
{
	return 0;
}
#endif

EFI_STATUS EFIAPI OemDtbbInit(VOID)
{
    EFI_STATUS      Status;
    UINT32        MaxShareBlockLength;
    UINT32        SharedAddr;

    Status = GetSharedAddress(&SharedAddr, &MaxShareBlockLength);
    if (EFI_ERROR(Status)) {
        goto OnError;
    }
    DEBUG_FUNC((0xffffffff, "SharedAddr = %x, MaxLength = %x\n", SharedAddr, MaxShareBlockLength));

    mSharedAhbAddr = SharedAddr;
    mSharedAhbSize = MaxShareBlockLength;
OnError:
    return Status;
}

EFI_STATUS OemDtbbEntryPoint(
    IN EFI_HANDLE ImageHandle,
    IN EFI_SYSTEM_TABLE *SystemTable
)
{
    EFI_STATUS    Status      = EFI_SUCCESS;

    InitAmiLib(ImageHandle, SystemTable);
    OemDtbbInit();

    Status = InstallDtbbProtocol();

    DEBUG_FUNC((0xffffffff, "InstallDtbbProtocol Status: %x\n", Status));

	InitData();

    // DtbbHelper_test();
    // do_all_test();
//OnError:
    return Status;
}
