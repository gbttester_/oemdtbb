#ifndef _OEM_DTBB_PROTOCOL_H_
#define _OEM_DTBB_PROTOCOL_H_

#include <Efi.h>
#include <AmiDxeLib.h>

#include "dtbb.h"

#define OEM_DTBB_PROTOCOL_VERSION           (0x0001)

struct _OEM_DTBB_PROTOCOL;
typedef struct _OEM_DTBB_PROTOCOL OEM_DTBB_PROTOCOL;

typedef
EFI_STATUS
(EFIAPI *DTBB_GET_SHARED_ADDRESS) (
  IN OEM_DTBB_PROTOCOL *This,
  OUT UINT32 *Address,
  OUT UINT32 *Length
);

typedef
EFI_STATUS
(EFIAPI *DTBB_MAP_SHARED_REGION) (
  IN OEM_DTBB_PROTOCOL *This,
  IN UINT32 Address,
  OUT UINT32 *HostAddress
);

typedef
EFI_STATUS
(EFIAPI *DTBB_DOORRING_HIT) (
  IN OEM_DTBB_PROTOCOL *This,
  IN UINT16            Length
);

typedef struct _OEM_DTBB_PROTOCOL {
    UINT16                  ProtocolVersion;
    DTBB_GET_SHARED_ADDRESS GetSharedAddress;
    DTBB_MAP_SHARED_REGION  MapSharedRegion;
    DTBB_DOORRING_HIT       HitDoorRing;
} OEM_DTBB_PROTOCOL;

// d2cb13b8-2c26-41f2-85fa-46352db626b3
#define OEM_DTBB_PROTOCOL_GUID { 0xd2cb13b8, 0x2c26, 0x41f2, \
  { 0x85, 0xfa, 0x46, 0x35, 0x2d, 0xb6, 0x26, 0xb3 } }


#define INIT_CC_FIELD(bf_req)		bf_req.compcode = 0xffffff

#define WINDOW_VIEW_SIZE(_p)			(0x10000 - (_p & 0xffff))

#define WAIT_FOR_DTBB_REQ(_bf_req)		{			\
		const UINT32 MaxRetry = 0x100; 					\
		UINT32	retry = 0;  						\
		do {										\
			pBS->Stall(1000*10); 					\
		}  while (_bf_req.compcode == 0xffffffff && retry++ <= MaxRetry);\
	}

//
// Helper
//
EFI_STATUS DtbbHelper_GetBmcNetworkRequest(
   IN OEM_DTBB_PROTOCOL	*This,
   IN OUT ip_conf	    **IpConf
);

EFI_STATUS DtbbHelper_SubmitBmcNetworkRequest(
   IN OEM_DTBB_PROTOCOL	*This,
   IN UINT8				Action,
   IN ip_conf	    	*IpConf
);

EFI_STATUS DtbbHelper_test(VOID);

#endif
