#define INTERNAL
#define APTIO
#if 0
#ifdef APTIO
#include "token.h"
#include <AmiLib.h>
#include <AmiDxeLib.h>

// [Yilun_T0497] +
#if DYNAMIC_PCIEXBASE_SUPPORT
#define PCIE_CFG_ADDR(bus,dev,func,reg) \
  ((VOID*) (UINTN)(GetPciBaseAddr() + ((bus) << 20) + ((dev) << 15) + ((func) << 12) + reg))
#else
// [Yilun_T0497] -
#define PCIE_CFG_ADDR(bus,dev,func,reg) \
      ((VOID*) (UINTN)(PCIEX_BASE_ADDRESS + ((bus) << 20) + ((dev) << 15) + ((func) << 12) + reg))
#endif                                                                                              // [Yilun_T0497]
#endif // APTIO
 
#include "ast1300.h"
#include "snb.h"

typedef UINT8   uint8_t;
typedef UINT16  uint16_t;
typedef UINT32  uint32_t;
typedef UINT64  uint64_t;

static uint8_t sio_read(uint8_t addr)
{
    IoWrite8(AST1300_CONF_ADDR, addr);
    return IoRead8(AST1300_CONF_DATA);
}

static void sio_write(uint8_t addr, uint8_t val)
{
    IoWrite8(AST1300_CONF_ADDR, addr);
    IoWrite8(AST1300_CONF_DATA, val);
}

static void sio_enter_cfg(void)
{
    IoWrite8(AST1300_CONF_BASE, 0xa5);
    IoWrite8(AST1300_CONF_BASE, 0xa5);
}

static void sio_leave_cfg(void)
{
    IoWrite8(AST1300_CONF_BASE, 0xaa);
}

static void 
set_l2a_bridge_en(uint8_t en)
{
    uint8_t val;
    sio_enter_cfg();
    sio_write(AST_SIOR_LDN,  AST_LPC2ABH_LDN);
    val = sio_read(AST_L2A_CONFIG);
    val |= 0x01;
    sio_write(AST_L2A_CONFIG, val);
}

void ast1300_ctx_enter(void)
{
    set_l2a_bridge_en(1);
}

void ast1300_ctx_leave(void)
{
    sio_leave_cfg();
}



static
void bridge_set_target_addr(uint32_t addr, AstL2aDataLength len)
{
    ast_l2a_data_length target_len;
    volatile uint8_t v;

    sio_write(AST_L2A_ADDR_01, addr >> 24); 
    sio_write(AST_L2A_ADDR_02, (addr >> 16) & 0xFF); 
    sio_write(AST_L2A_ADDR_03, (addr >> 8) & 0xFF); 
    v = addr & 0xff;
    sio_write(AST_L2A_ADDR_04, v); 
    //printf("addr0 = %x\n",  addr&0xff);
    target_len.v = sio_read(AST_L2A_DATA_LEN);
    target_len.Length = len;
    sio_write(AST_L2A_DATA_LEN, target_len.v);
}

uint32_t bridge_read_word(uint32_t addr) {

    bridge_set_target_addr(addr, FourBytes);
    sio_read(AST_L2A_REQUEST);
    // FIXME: To add delay.
    return (sio_read(AST_L2A_DATA_04) |
            (sio_read(AST_L2A_DATA_03) << 8) |
            (sio_read(AST_L2A_DATA_02) << 16) |
            (sio_read(AST_L2A_DATA_01) << 24));
}

void bridge_write_word(uint32_t addr, uint32_t v) 
{
    bridge_set_target_addr(addr, FourBytes);
    // FIXME: To add delay.
    sio_write(AST_L2A_DATA_04, (v & 0xFF));
    sio_write(AST_L2A_DATA_03, (v >> 8) & 0xFF);
    sio_write(AST_L2A_DATA_02, (v >> 16) & 0xFF);
    sio_write(AST_L2A_DATA_01, (v >> 24) & 0xFF);
    sio_write(AST_L2A_REQUEST, AST_WRITE_REQUEST);
    sio_write(AST_L2A_REQUEST, AST_WRITE_REQUEST);
    sio_write(AST_L2A_REQUEST, AST_WRITE_REQUEST);
}


uint16_t read_adc(uint8_t idx)
{
    uint32_t addr = AST_SRAM_BASE + ADC_OFFSET + 0x08 + 4*idx;
    uint32_t val;
    val = bridge_read_word(addr) & 0x03FF;
    return val;
}

uint32_t read_tach(uint8_t idx)
{
    uint32_t addr = AST_SRAM_BASE + TACO_OFFSET + 0x08 + 4*idx;
    uint32_t val;
    val = bridge_read_word(addr);
    return val;
}

uint32_t read_peci_temp(uint8_t idx)
{
    uint32_t addr = AST_SRAM_BASE + DATA_OFFSET + 0x50 + 4*idx;
    return bridge_read_word(addr);
}

uint32_t read_i2c_temp(uint8_t pwm_idx, uint8_t i2c_idx)
{
    uint32_t addr = AST_SRAM_BASE + DATA_OFFSET + 0x80 + pwm_idx * 0x14 + i2c_idx*4;

    return bridge_read_word(addr);
}

uint32_t read_fw_ver(void)
{
    uint32_t addr = AST_SRAM_BASE + DATA_OFFSET + 0x1c0;
    return bridge_read_word(addr);
}

uint32_t check_fw_signature(void)
{
    uint32_t addr = AST_SRAM_BASE + DATA_OFFSET + 0x1c8;
    return bridge_read_word(addr) == 0x55aa55aa;
}


// @idx: PMW 0-based index.
// @freq: Frequency in Hz.
void set_pwm_freq(uint8_t idx, uint32_t freq)
{
    uint32_t addr = AST_SRAM_BASE + PWM_OFFSET + 0x08 + 4*idx;
    bridge_write_word(addr, freq);
}

// @idx: PMW 0-based index.
// @duty: Duty in percentage.
void set_pwm_duty(uint8_t idx, uint8_t duty)
{
    uint32_t addr;
    uint8_t val;
    uint8_t group_idx = idx / 4; 
    uint8_t minor_idx = idx % 4;

    addr = AST_SRAM_BASE + PWM_OFFSET + 0x28 + 4*group_idx;
    val = bridge_read_word(addr);
    val &= ~(0xff << (8*minor_idx));
    val |= (duty << (8*minor_idx));
    bridge_write_word(addr, val);
}

uint8_t get_pwm_duty(const uint8_t idx)
{
    uint32_t addr;
    uint8_t val;
    uint8_t group_idx = idx / 4; 
    uint8_t minor_idx = idx % 4;

    addr = AST_SRAM_BASE + PWM_OFFSET + 0x28 + 4*group_idx;
    val = bridge_read_word(addr);
    return (val &&(0xff << (8*minor_idx))) >> (8*minor_idx);
}

void _enable_i2c_ch(const uint8_t bitmap)
{
    uint32_t addr;
    addr = AST_SRAM_BASE + I2C_OFFSET;
    bridge_write_word(addr, bitmap);
}

void enable_i2c_ch(const uint8_t bitmap)
{
    ast1300_ctx_enter();
    _enable_i2c_ch(bitmap);
    ast1300_ctx_leave();

}

void _change_i2c_freq(const uint8_t ch_idx, const uint32_t freq)
{
    uint32_t addr;
    addr = AST_SRAM_BASE + I2C_OFFSET + 0x30 + ch_idx * 4;

    bridge_write_word(addr, freq);
}

void change_i2c_freq(const uint8_t ch_idx, const uint32_t freq)
{
    ast1300_ctx_enter();
    _change_i2c_freq(ch_idx, freq);
    ast1300_ctx_leave();
}


void _set_peci_tjmax(const uint8_t idx, const uint32_t temp)
{
    uint32_t addr;
    uint32_t val;
    addr = AST_SRAM_BASE + PWM_OFFSET + 0xE0 + 4*idx;
    
    val = bridge_read_word(addr);
    val &= ~0xff000000; 
    val |= temp << 24;
    bridge_write_word(addr, val);
}

void set_peci_tjmax(const uint8_t idx, const uint32_t temp)
{
    ast1300_ctx_enter();
    _set_peci_tjmax(idx, temp);
    ast1300_ctx_leave();
}

void _init_fan_table(uint8_t idx, uint8_t *tbl)
{
    uint32_t addr;
    uint32_t l;
    addr = AST_SRAM_BASE + PWM_OFFSET + 0x100 + idx * 0x40;
   
    for (l= 0; l!= 0x40; l += 4)  {
        bridge_write_word(addr + l, *(uint32_t*)(&tbl[l]));
    }

}

void init_fan_table(uint8_t idx, uint8_t *tbl)
{
    ast1300_ctx_enter();
    _init_fan_table(idx, tbl);
    ast1300_ctx_leave();
}

// TODO: SmartFan. However AST1300 provides setting tables.
#endif

