#include "InitDataTableDxe.h"

EFI_STATUS InitFan(
   IN VOID      *Context
)
{
    EFI_STATUS      Status;

    Status = OemDtbbInit();

    if (EFI_ERROR(Status)) {
        TRACE((-1, "Failed to OemDtbbInit, Status:%x\n", Status));
        return Status;
    }

    Status = InitData();

    return Status;
}

static HWM_INIT_FAN_PROTOCOL HwmInitFanProtocol = {
    &InitFan
};

EFI_STATUS
InitDataTableDxeEntryPoint(
   IN EFI_HANDLE        ImageHandle,
   IN EFI_SYSTEM_TABLE  *SystemTable)
{
    EFI_STATUS          Status;
    EFI_HANDLE          Handle = NULL;
    static EFI_GUID     gHwmFanInitGuid = HWM_FAN_INIT_GUID;

    InitAmiLib(ImageHandle, SystemTable);
#if _OEM_DTBB_DISABLED
    return 0;
#endif

    Status = pBS->InstallMultipleProtocolInterfaces(&ImageHandle,
                                                    &gHwmFanInitGuid, &HwmInitFanProtocol,
                                                    NULL, NULL);

    if (EFI_ERROR(Status)) {
        TRACE((-1, "InstallMultipleProtocolInterfaces Status:%x\n", Status));
        return Status;
    }

    InitFan(NULL);

    return Status;
}
