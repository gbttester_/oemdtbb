ALL: InitDataTableDxe

InitDataTableDxe: $(BUILD_DIR)\InitDataTableDxe.mak InitDataTableDxeBin

$(BUILD_DIR)\InitDataTableDxe.mak: $(INIT_DATA_TABLE_DXE_DIR)\$(@B).cif $(INIT_DATA_TABLE_DXE_DIR)\$(@B).mak
	$(CIF2MAK) $(INIT_DATA_TABLE_DXE_DIR)\$(@B).cif $(CIF2MAK_DEFAULTS)

INIT_DATA_TABLE_DXE_CFLAGS = $(CFLAGS) /I $(OEM_DTBB_DIR)

InitDataTableDxeBin: $(AMIDXELIB) $(AMICSPLib) $(AspeedHwm_LIB)
	$(MAKE) /$(MAKEFLAGS) $(BUILD_DEFAULTS)\
		/f $(BUILD_DIR)\InitDataTableDxe.mak all \
		TYPE=BS_DRIVER\
		"CFLAGS=$(INIT_DATA_TABLE_DXE_CFLAGS)"\
		"GUID=6086DF96-E7AB-4183-9954-9AF8B3192AA5"\
		"ENTRY_POINT=InitDataTableDxeEntryPoint"\
		DEPEX1=$(INIT_DATA_TABLE_DXE_DIR)\InitDataTableDxe.dxs DEPEX1_TYPE=EFI_SECTION_DXE_DEPEX\
		COMPRESS=1
