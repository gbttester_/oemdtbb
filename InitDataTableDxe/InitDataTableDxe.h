#ifndef __INIT_DATA_TABLE_DXE_H__
#define __INIT_DATA_TABLE_DXE_H__
#include <Token.h>
#include <AmiDxeLib.h>
#include "AspeedHwm.h"
#include "crc32.h"

//6c5aa800-cfd2-487b-94c1-a43a59a33dc1
#define HWM_FAN_INIT_GUID { 0x6c5aa800, 0xcfd2, 0x487b, \
  { 0x94, 0xc1, 0xa4, 0x3a, 0x59, 0xa3, 0x3d, 0xc1 } }

typedef struct _HWM_INIT_FAN_PROTOCOL HWM_INIT_FAN_PROTOCOL;

typedef EFI_STATUS (*HWM_INIT_FAN_INIT)(
	IN HWM_INIT_FAN_PROTOCOL		*This
);

typedef struct _HWM_INIT_FAN_PROTOCOL {
		HWM_INIT_FAN_INIT  InitFan;
} HWM_INIT_FAN_PROTOCOL;

#endif