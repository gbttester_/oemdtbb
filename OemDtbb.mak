X2COMP_NAME=OemDtbb

ALL: OemDtbb

OemDtbb: $(BUILD_DIR)\OemDtbb.mak OemDtbbBin

# /FAcs
# /Os /Oy /Ob2 /GF /Gy\

OEM_DTBB_CFLAGS=$(CFLAGS) \
 /FAcs \
 /W3 \
 /wd4255 /wd4201 /wd4820 /wd4115 /wd4668 \
 -I $(OEM_DTBB_DIR) \
 -I Include\
 -I EDK\Foundation\Include\
 -I $(PROJECT_DIR)\

!IF "$(IPMI_SUPPORT)" == "1"
OEM_DTBB_CFLAGS=$(OEM_DTBB_CFLAGS) \
	 -I Core\Em\Ipmi\Include \
	 -I Core\Em\Ipmi\Include\Protocol
!ELSE
!ENDIF

OEM_DTBB_EXTRA_OBJS =
#$(BUILD_DIR)\DynamicPciBaseCspLib.lib

DTBB_LDFLAGS=

# /OPT:REF /MAP /OPT:ICF=100 \
# /VERBOSE:LIB /VERBOSE:REF /VERBOSE:ICF \
# /INCREMENTAL:NO

$(BUILD_DIR)\OemDtbb.mak : $(OEM_DTBB_DIR)\$(X2COMP_NAME).cif $(OEM_DTBB_DIR)\$(X2COMP_NAME).mak $(BUILD_RULES)
	$(CIF2MAK) $(OEM_DTBB_DIR)\$(X2COMP_NAME).cif $(CIF2MAK_DEFAULTS)


### Standalone need $(AMIDXELIB) $(AMICSPLib)
AspeedHwm_LIB: $(BUILD_DIR)\OemDtbb.mak OemDtbbBin

$(AspeedHwm_LIB): $(BUILD_DIR)\OemDtbb.mak OemDtbbBin

OemDtbbBin :
	$(MAKE) /$(MAKEFLAGS) $(BUILD_DEFAULTS)\
		EXT_OBJS=$(OEM_DTBB_EXTRA_OBJS)\
		/f $(BUILD_DIR)\$(X2COMP_NAME).mak all\
		"MY_INCLUDES=$(OEM_DTBB_CFLAGS)"\
		TYPE=LIBRARY\
		LIBRARY_NAME=$(AspeedHwm_LIB)
