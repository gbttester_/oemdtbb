/*
 * Functions get_smb_info() and smb_transfer() involve accessing same file.
 * They are not thread-safe so ensure not to call them in different threads.
 * In fact, better to use them only in OEMCmds.c
 */

#ifndef DTBB_H
#define DTBB_H

// typedef unsigned char u8;
// typedef unsigned short u16;
// typedef unsigned int u32;
//
enum {
    SM_ACTION_NONE,
    SM_ACTION_GET,
    SM_ACTION_SET,
    SM_ACTION_START,
    SM_ACTION_STOP,
    SM_ACTION_RESTART,
    SM_ACTION_COMPLETE,
    SM_ACTION_CLEAR,
    SM_ACTION_INIT,
    SM_ACTION_MAX = 128
};


typedef UINT8    u8;
typedef UINT16   u16;
typedef UINT32   u32;
#pragma pack(push, 1)
/* common data transfer for BIOS and BMC */
typedef struct {
    u32 size;
    u8 value[188];
} transfer_data;

/* related information of CPU */
typedef struct {
    UINT8   Reserved;
	// TODO: in future
} cpu_info;

/* related infomation of network, include IPv4 and IPv6 */
// FIXME: ipv4 & ipv6
typedef struct {
    /* For IPv4: lan_info[0] = Channel number; lan_info[1] = IP Address Source
       For IPv6: lan_info[0] = LAN interface status; lan_info[1] = Auto configuration option */
    u8 lan_info[4];
    u8 ip_addr[48];
    u8 netmask[16];
    u8 gateway[48];
    u8 link_local[48];
} ip_conf;

/* related infomation of version, include sdr, fru, fw version */
typedef struct {
    u8 sdr[4];
    u8 fru[4];
    u8 fw[4];
} ver_info;

/* related infomation of BIOS update */
typedef struct {
    UINT8   Reserved;
	// TODO: in future
} bios_update;

/* related infomation of BIOS request large data, include index and length */
typedef struct {
    u32 sel_len;
    u32 sel_index;
    u32 fru_len;
    u32 fru_index;
} bios_req_large_data;

#define SIO_EC_EACH_BANKS   4
typedef struct {
    u8 id[4];            // Who filled this table ? BIOS or BMC
    struct _control {
        u8 bus_media;    // [7:4] Bus/Channel
                         // [3:0] Media type
        u8 addr;         // Address of I2C and PECI
        u8 data_addr[2]; // Data0~1 address
        u8 data_len;     // [7:4] the data length of writing
                         // [3:0] the data length of reading
        u8 ex_idx;       // [7:4] I/O or I2C expander index
                         //       0x0f: Disable
                         //       0x01 ~ 0x0f: a index of control table.
                         // [3:0] Port Number
        u8 data[10];     // I2C data/PECI parameter
    } control[10];

    u16 io_buff[10];
} ec_t;

typedef struct
{
    u8 id[4];            // Who filled this table ? BIOS or BMC
    struct fsc {
         u8  idx;
         u8  algorithm;
         u8  step;
         u8  fan[13];
         u8  sensor[16];
         u8  temp_rise[8];
         u8  temp_fall[8];
         u16 duty_table[8];
         u8  type_chassis;
         u8  type_hdd;
         u8  type_reserve;
         u8  type_cpu;
         u16 type_tdp[6];
    } policy[3];
} fsc_sm_t;

#define DTBB_ACTION_NULL	    0
#define DTBB_ACTION_GET			1
#define DTBB_ACTION_SET			2
#define DTBB_ACTION_START		3
#define DTBB_ACTION_STOP		4
#define DTBB_ACTION_RESTART		5
#define DTBB_ACTION_COPLETE		6
#define DTBB_ACTION_CLEAR		7
//__declspec(align(256)) 

/* every infomation struct, total size of share_memory : 256 bytes */
typedef struct {
    u32 action; // 1:get, 2:set, 3:start, 4:stop, 5:restart, 6:complete, 7:clear
    u32 mode; // 0:Hand Shacking mode, 1: Real Time mode
    u32 compcode;
    union data {
        transfer_data td;
        cpu_info cpu;
        ip_conf ip;
        ver_info ver;
        bios_update ud;
        bios_req_large_data req;
        ec_t ec;
        fsc_sm_t fsc_sm;			
    } un;
    //u8 reserved[256-sizeof(u32)*3-sizeof(union data)];
} share_memory;

typedef struct _dummy_check {
	u8  reserved[256-sizeof(u32)*3-sizeof(union data)];
} dummy_check;

enum {
    SM_POSTCODE = 0,
    SM_FRB3,
    SM_CHASSIS,
    SM_SENSOR,
    SM_IPV4,
    SM_IPV6,
    SM_VERSION,
    SM_BIOS_UPDATE,
    SM_BIOS_REQ_INFO,
    SM_SIO_EC_VIN0,     //  900
    SM_SIO_EC_VIN1,
    SM_SIO_EC_VIN2,
    SM_SIO_EC_VIN3,
    SM_SIO_EC_TEMP0,    //  d00
    SM_SIO_EC_TEMP1,
    SM_SIO_EC_TEMP2,
    SM_SIO_EC_TEMP3,
    SM_SIO_EC_TACHO0,   // 1100
    SM_SIO_EC_TACHO1,
    SM_SIO_EC_TACHO2,
    SM_SIO_EC_TACHO3,
    SM_SIO_EC_PWM0,     // 1500
    SM_SIO_EC_PWM1,
    SM_SIO_EC_PWM2,
    SM_SIO_EC_PWM3,
    SM_SIO_EC_I2CDEV0,  // 1900
    SM_SIO_EC_I2CDEV1,
    SM_SIO_EC_I2CDEV2,
    SM_SIO_EC_I2CDEV3,
    SM_SIO_FSC0,        // 1d00
    SM_SIO_FSC1,
    SM_SIO_FSC2,
    SM_SIO_FSC3,
    SM_SIO_FSC4,
    SM_MAX = 256
};

/* BIOS request table */
typedef struct {
    share_memory postcode;		// 0
    share_memory frb3;			// 1
    share_memory chassis;		// 2
    share_memory sensor;
    share_memory ipv4;
    share_memory ipv6;
    share_memory version;
    share_memory bios_update;
    share_memory req_info;
    share_memory sio_vin[SIO_EC_EACH_BANKS];
    share_memory sio_temp[SIO_EC_EACH_BANKS];
    share_memory sio_fan[SIO_EC_EACH_BANKS];
    share_memory sio_pwm[SIO_EC_EACH_BANKS];
    share_memory i2cdev_init[SIO_EC_EACH_BANKS];
    share_memory sio_fsc[5];
} bios_form;
#pragma pack(pop)

#if 0
typedef u32 (*handler_function) (share_memory *, int);

typedef struct {
    /** Pointer to the call back function when power state change */
    handler_function handler_function_callback;

    u8 handler_function_id;

    /** Pointer to the next service */
    void *next_handler;
}mem_handler;


u8 get_dtbb_info(u8 *rsp, u8 *rsp_len);
u8 bios_req_handler(const u8 *req, u8 *rsp, u8 *rsp_len);
u8 get_smb_info(const u8 type, const u8 id, u8 *rsp, u8 *rsp_len);

u8 get_bios_ver(u8 *rsp, u8 *rsp_len);
u8 get_bios_date(u8 *rsp, u8 *rsp_len);

u8 sm_selftest_send(bios_form *);

void handler_function_register_callback_function(void *handler);
#endif

#endif
