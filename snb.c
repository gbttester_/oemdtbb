#define APTIO
#include "snb.h"

#define MSR_TEMPERATURE_TARGET                  0x1A2

extern UINT64 ReadMsr(IN UINT32 Msr);

// [Yilun_T0497] +
#if DYNAMIC_PCIEXBASE_SUPPORT
#define PCIE_CFG_ADDR(bus,dev,func,reg) \
  ((VOID*) (UINTN)(GetPciBaseAddr() + ((bus) << 20) + ((dev) << 15) + ((func) << 12) + reg))
#else
// [Yilun_T0497] -
#define PCIE_CFG_ADDR(bus,dev,func,reg) \
	  ((VOID*) (UINTN)(PCIEX_BASE_ADDRESS + ((bus) << 20) + ((dev) << 15) + ((func) << 12) + reg))
#endif																								// [Yilun_T0497]

uint32_t read_tdp(uint8_t node)
{
	// uint64_t* addr;
	uint32_t data;
	// uint32_t unit;

	data = ReadMsr(0x614) & 0x7FFF;

	return data / 8;
	#if 0
	package_power_sku* power_sku;
	package_power_sku_unit* unit;

	addr = PCIE_CFG_ADDR(0xff, 10, 0, 0x84);
	power_sku = (package_power_sku*)addr;
	addr = PCIE_CFG_ADDR(0xff, 10, 0, 0x8C);
	unit = (package_power_sku_unit*)addr;

	return power_sku->package_tdp_power >> unit->power_unit;
	#endif
}

//
// Read TjMax
//
uint64_t read_tjmax(uint8_t node)
{
    uint64_t val;
    val = ReadMsr(MSR_TEMPERATURE_TARGET);
    return (val >> 16) & 0xFF;
}

uint64_t nr_cpu = 0; 

static init_cpu_info(void)
{
	nr_cpu = (*(uint32_t*)PCIE_CFG_ADDR(0x80, 0, 0, 0) == -1) ? 1: 2;
}

uint64_t read_dimmtemp(uint8_t node, uint8_t ch_idx)
{
    uint32_t v;
    uint8_t uncore_bus;
    if (nr_cpu == 0)
        init_cpu_info();

    if (nr_cpu == 1) {
        uncore_bus = 0xff;
    } else if (nr_cpu == 2) {
        uncore_bus = (node == 0)? 0x7f: 0xff;
    }
    v = *(uint32_t*)(PCIE_CFG_ADDR(uncore_bus, 15, 0, 0x120)) ;
    if (ch_idx == 0) {
        if (v & (1 << 15)) 
            return  v & 0xff;
        else
            return -1; 
    } else if (ch_idx == 1) {
        if (v & (1 << 31)) 
            return  (v>>16) & 0xff;
        else
            return -1;
    }
    return -1;
}


