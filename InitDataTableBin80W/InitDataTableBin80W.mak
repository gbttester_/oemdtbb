ALL: InitDataTable

InitDataTable: InitDataTableBin

INIT_DATA_TABLE_GUID = D6748710-BEFE-4456-B6BD-DCE05494AC1A

InitDataTableBin: $(BUILD_DIR)\InitDataTableBin80W.ffs

$(BUILD_DIR)\InitDataTableBin80W.ffs: $(INIT_DATA_TABLE_80W_BIN_FILE)
	$(MAKE) /f Core\FFS.mak \
		BUILD_DIR=$(BUILD_DIR) \
        GUID=$(INIT_DATA_TABLE_GUID)\
		TYPE=EFI_FV_FILETYPE_RAW \
        FFS_ALIGNMENT=1 FFS_CHECKSUM=1\
		RAWFILE=$** FFSFILE=$@ COMPRESS=0 NAME=$(**B)
