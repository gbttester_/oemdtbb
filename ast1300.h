#ifndef __AST_1300_OEM_DTBB_H__
#define __AST_1300_OEM_DTBB_H__

#ifdef APTIO
#include "token.h"
#include <AmiLib.h>
#include <AmiDxeLib.h>
#endif

typedef UINT8   uint8_t;
typedef UINT16  uint16_t;
typedef UINT32  uint32_t;


#if !defined(__AST2300_H__)
#define __AST2300_H__
#define AST1300_CONF_BASE               0x4e
#define AST1300_CONF_ADDR               (AST1300_CONF_BASE  )
#define AST1300_CONF_DATA               (AST1300_CONF_BASE + 1  )
#define AST_SIOR_LDN                    (0x07)

typedef union {
        struct {
                uint8_t Ldn:4;                                  // Logical Device Number
                uint8_t Reserved:4;
        };
        uint8_t v;
} ast_sior_ldn;
#define AST_LPC2ABH_LDN                 (0x0D)


#define AST_L2A_CONFIG                  (0x30)
typedef union {
        struct {
                uint8_t L2aEnable:1;
                uint8_t SmiEnable:1;
                uint8_t SmiPolarity:1;
                uint8_t SvidMode:1;
                uint8_t SioScratchEnable:1;
                uint8_t Reserved:3;
        };
        uint8_t v;
} ast_l2a_config;

#define AST_L2A_ADDR_01                 (0xF0)
#define AST_L2A_ADDR_02                 (0xF1)
#define AST_L2A_ADDR_03                 (0xF2)
#define AST_L2A_ADDR_04                 (0xF3)

#define AST_L2A_DATA_01                 (0xF4)
#define AST_L2A_DATA_02                 (0xF5)
#define AST_L2A_DATA_03                 (0xF6)
#define AST_L2A_DATA_04                 (0xF7)

#define AST_L2A_DATA_LEN                (0xF8)
typedef enum {
        OneByte,
        TwoBytes,
        FourBytes
} AstL2aDataLength;

typedef union {
        struct {
                AstL2aDataLength Length:2;
                uint8_t Reserved:6;
        };
        uint8_t v;
} ast_l2a_data_length;


#define AST_L2A_REQUEST                 (0xFE)
#define AST_WRITE_REQUEST               (0xCF)


#define VUART_BASE                              (0x1E787000)
#define PTCR_BASE                               0x1E786000
#define RTC_BASE                                0x1E781000
#define SCU_BASE                                0x1E6E2000
#define ADC_BASE                                0x1E6E9000

#define SCU_UNLOCK_KEY                          0x1688A8A8
#define SCU_ADC_RESET_BIT                       (BIT23)
#define SCU_PWM_RESET_BIT                       (BIT9)


#define LPC_BASE                                0x1E789000

#define HICR9_REG                               0x98
#define HICRA_REG                               0x9C


#define HISR1                                   0x10C
//
// Aspeed released Coldfire FW related. Don't assume they are always true.
//
#define AST_SRAM_BASE                           0x1e720000
#define ADC_OFFSET                              0x1600
#define PWM_OFFSET                              0x1900
#define TACO_OFFSET                             0x1500
#define DATA_OFFSET                             0x2000
#define I2C_OFFSET                              0x1700

#endif



#define ADC_INTERPRET(r1,r2, vr, v2)            (r2 != 0)?((((vr)*2500)*((r1)+(r2))/(1024*(r2))) - (r1)*(v2)/(r2)) : (((vr)*2500)/1024)
#define ADC_INTERPRET2(r1,r2, vr)               (r2 != 0)?((((vr)*2500)*((r1)+(r2))/(1024*(r2)))) : (((vr)*2500)/1024)

#define ADC_INTERPRET3(r1,r2, vr)               (r2 != 0)? (((vr) * 1000 / 1024) * ((r1) + (r2))/ (r2)) : ((vr) * 1000 / 1024)

#define ADC_ITE_INTERPRET(vr)                   ((vr)*3072/(256))

#ifndef INTERNAL
void ast1300_ctx_enter(void);
void ast1300_ctx_leave(void);
uint32_t bridge_read_word(uint32_t addr);
void bridge_write_word(uint32_t addr, uint32_t v);
#endif

#define INTERNAL_USE(x)                             INT32 _##x##used = 1;
#define INTERNAL_USE2(x)                             _##x##used = 1;
#define INTERNAL_USE_DECL(x)                        extern INT32 _##x##used;
#define _INTERNAL_USE(x)                            (void) _##x##used;

#ifndef _INTERNAL_
    #define _INTERNAL_          _INTERNAL_USE(flt)
#endif

#if defined(APTIO) && defined(INTERNAL)
    GLOBAL_REMOVE_IF_UNREFERENCED INTERNAL_USE(flt)
    GLOBAL_REMOVE_IF_UNREFERENCED INTERNAL_USE_DECL(flt)
#endif

    #if !defined(INTERNAL_USE_DECL__)
    #define INTERNAL_USE_DECL__
    static void __Foo__(void) {
        INTERNAL_USE2(flt)
    };
    #define __FP_USED__ __Foo__();
    #endif

#endif
